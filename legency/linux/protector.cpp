#include "protector.h"


DllExport int stest(int x) {
	return x + x;
}

int APP_PROTECTOR_FUNC test(int x)
{
	static int k = 3;
APP_PROTECTOR_START(test)
	x = x+ 2 * k;
	int y = stest(x);
APP_PROTECTOR_END(test)

		return y+1;
}

int APP_PROTECTOR_FUNC test1(int x)
{
	static int k = 3;
APP_PROTECTOR_START(test1)
APP_PROTECTOR_CHECKTIME();
	x = x+ 2 * k;
	int y = stest(x);
APP_PROTECTOR_END(test1)

		return y+1;
}


DllExport extern "C" int bar(int x){
	return x+test(x) + test1(x);
}

