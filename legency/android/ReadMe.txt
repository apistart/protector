Android 平台下的测试代码

protector: 用于生成 .so 动态链接库
test.c： 测试代码的主程序

用法：
1. 将 install/bin 下面的的 libprotector.a 静态链接库以及 install/include下的 protector.h 头文件拷贝在此目录
2. $ make
3. 加密： $ ruby ../../encrpyt/android/app_protect.rb protector.so 
4. 加密后执行，需要将 test 和 protector.so 拷贝到 ARM 平台上方可执行
