require 'sinatra'
require 'json'
require "base64"
require 'rc4'

set :port ,8010
post '/ping' do
	key = "SenseTime-API"
	rc4 = RC4.new key
	t = rc4.decrypt(Base64.decode64(request.body.read))
	p JSON.parse(t)
end
