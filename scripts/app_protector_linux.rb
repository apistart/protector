#!/usr/bin/env ruby

require 'BFD'
require 'openssl'
require 'fileutils'
require 'pp'

require_relative 'protect_common'

def calc_phy_addr bfd, sym
  #sec = bfd.sections[sym.section]
  #p sec.file_pos.to_s(16)
  #symbol value == elf file offset
  return sym.value
end

def protect_part io, bfd, part
  start_pa = calc_phy_addr bfd, part[:start]
  fill_pa = calc_phy_addr bfd, part[:fill]
  end_pa = calc_phy_addr bfd, part[:end]
  tot_size = end_pa - start_pa
  size = (fill_pa - start_pa + MAX_RSA_SIZE - 1) / MAX_RSA_SIZE * MAX_RSA_SIZE
  fail if size <= 0
  STDERR.puts "#{part[:name]} size: 0x#{size.to_s(16)}"

  public_key = OpenSSL::PKey::RSA.new File.read(PUBLIC_KEY)
  private_key = OpenSSL::PKey::RSA.new File.read(PRIVATE_KEY)
  io.seek start_pa
  magic = io.read(4).unpack("L")[0]
  origin_magic = 0x5b535850 if $elf_type == "Linux"
  origin_magic = 0xe1a00000 if $elf_type == "Arm"
  origin_magic = 0xd503201f if $elf_type == "Aarch64"
  unless magic == origin_magic
    STDERR.puts "#{part[:name]} has already encrypted!"
    return
  end
  io.seek start_pa
  orig = io.read size

  cipher = ""
  orig.bytes.each_slice(MAX_RSA_SIZE) {|e|
    #cipher += public_key.public_encrypt(e.pack("C*"))
    cipher += private_key.private_encrypt(e.pack("C*"))
  }
  p cipher.size
  fail "Cipher too large" if cipher.size > tot_size
  io.seek start_pa

  #File.open("test.bin", "wb") {|io1| io1.write cipher}
  io.write cipher
end

def __set_crt_field io, bfd, offset, buf
  crtfct = "crtfct"
  pa = calc_phy_addr bfd, bfd.symbols[crtfct]
  pa = pa - bfd.sections[bfd.symbols[crtfct].section].vma + bfd.sections[bfd.symbols[crtfct].section].file_pos
  pa += offset
  io.seek pa
  io.write(buf)
end

def set_time io, bfd, options
  if options[:start_date] && options[:end_date]
    buf = [symm_encrypt(options[:end_date]), symm_encrypt(options[:start_date])].pack("LL")   # first one is end_time
    __set_crt_field io, bfd, TIME_OFFSET, buf
  end
end

def set_tag io, bfd, tag
  __set_crt_field io, bfd, TAG_OFFSET, tag
end

def set_appid io, bfd, appid
  buf = symm_encrypt_str(appid)+[0].pack("C*")
  __set_crt_field io, bfd, APPID_OFFSET, buf
end

def set_private_key io, bfd, crtfct
  pa = calc_phy_addr bfd, bfd.symbols[crtfct]
  pa = pa - bfd.sections[bfd.symbols[crtfct].section].vma + bfd.sections[bfd.symbols[crtfct].section].file_pos
  buf = File.read(ENCRYPTED_PRIVATE_KEY)
  io.seek pa
  io.write(buf)
end

options = ProtectorOptParser.parse!(ARGV)
input_f = ARGV[0]
#p options

f = Bfd::Target.new(input_f)
unless f.symbols["crtfct"]
  STDERR.puts "warning: crtfct not found"
  exit 0
end

marks = f.symbols.select {|k,v| k =~ /__appProtect_.+/}
parts = {}
marks.each {|k,v|
  next unless k =~ /__appProtect_(.+?)_start/
  name = $1
  t = {
    :name => name,
    :start => marks["__appProtect_#{name}_start"],
    :fill => marks["__appProtect_#{name}_fill"],
    :end => marks["__appProtect_#{name}_end"],
  }
  parts[name] = t unless t.has_value?(nil)
}

type = f.type

if type == 'elf64-x86-64'
  $elf_type = 'Linux'
elsif type == 'elf32-little'
  $elf_type = $elf_type || 'Arm'
elsif type == 'elf64-little' # TODO use aarch64 flags
  $elf_type = 'Aarch64'
else
  fail "Unsupported ELF format"
end

tmp_f = input_f + ".tmp"
FileUtils.cp(input_f, tmp_f)

outf = File.open(tmp_f, "rb+") {|io|
  set_time io, f, options
  set_tag io, f, options[:tag] if options[:tag]
  set_appid io, f, options[:appid] if options[:appid]

  set_private_key io, f, "crtfct" if SET_PRIVATE_KEY == 1
  parts.each {|k,v| protect_part io, f, v} if options[:encrypted]
}

FileUtils.mv(tmp_f, input_f) if options[:overwrite]

