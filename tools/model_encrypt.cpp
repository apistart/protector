#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include "protector/protector.h"

int data_encrypt(const char* data_src, char* data_dst);
int data_decrypt(const char* data_src, char* data_dst);

void Usage()
{
	std::cout << "input params error" << std::endl;
	std::cout << "run this exe as following command line:" << std::endl;
	std::cout << "model_encrypt.exe arg1 arg2 arg3" << std::endl;
	std::cout << "arg1: a option parameter, only for -encrypt and -decrypt" << std::endl;
	std::cout << "arg2: src data file, such as deepface_model.bin" << std::endl;
	std::cout << "arg3: dst data file, such as deepface_model_encrypt.bin" << std::endl;
}

int main(int argc, char* argv[])
{
	if (argc != 4) {
		Usage();
	} else {
		std::string tmp(argv[1]);
		if (tmp == "-encrypt") {
			std::cout << "data encrypt" << std::endl;
			data_encrypt(argv[2], argv[3]);
		} else if (tmp == "-decrypt") {
			std::cout << "data decrypt" << std::endl;
			data_decrypt(argv[2], argv[3]);
		} else {
			Usage();
		}
	}

	return 0;
}

int data_encrypt(const char* data_src, char* data_dst)
{
	FILE* stream_src = fopen(data_src, "rb");
	if (!stream_src) {
		std::cout << "open src data file error" << std::endl;
		std::cout << "data encrypt fail" << std::endl;
		return -1;
	}
	fseek(stream_src, 0, SEEK_END);
	int file_size = ftell(stream_src);

	unsigned char* encrypted_file = NULL;
	unsigned char* buf = NULL;
	buf = (unsigned char*)malloc(sizeof(unsigned char) * file_size);
	fseek(stream_src, 0, SEEK_SET);
	fread(buf, 1, file_size, stream_src);

	int encrypt_len = STEF_encrypt2mem(buf, buf + file_size, &encrypted_file);
	if (encrypt_len < 0) {
		std::cout << "failed to encrypt data" << std::endl;
		std::cout << "data encrypt fail" << std::endl;
		fclose(stream_src);
		free(buf);
		return -1;
	}

	FILE* stream_dst = fopen(data_dst, "wb");
	if (!stream_dst) {
		std::cout << "open dst data file error" << std::endl;
		std::cout << "data encrypt fail" << std::endl;
		fclose(stream_src);
		free(buf);
		free(encrypted_file);
		return -1;
	}

	fwrite(encrypted_file, 1, encrypt_len, stream_dst);

	fclose(stream_src);
	fclose(stream_dst);
	free(buf);
	free(encrypted_file);

	std::cout << "data encrypt success" << std::endl;

	return 0;
}

int data_decrypt(const char* data_src, char* data_dst)
{
	if (STEF_is_encrypt_file(data_src)) {
		FILE* stream_src = fopen(data_src, "rb");
		if (!stream_src) {
			std::cout << "open src data file error" << std::endl;
			std::cout << "data decrypt fail" << std::endl;
			return -1;
		}

		fseek(stream_src, 0, SEEK_END);
		int file_size = ftell(stream_src);
		unsigned char* buf = NULL;
		buf = (unsigned char*)malloc(sizeof(unsigned char) * file_size);

		fseek(stream_src, 0, SEEK_SET);
		fread(buf, 1, file_size, stream_src);
		unsigned char* decrypted_file = NULL;

		int decrypt_len = STEF_decrypt2mem(buf, buf + file_size, &decrypted_file);
		if (decrypt_len < 0) {
			std::cout << "failed to decrypt data" << std::endl;
			std::cout << "data decrypt fail" << std::endl;
			fclose(stream_src);
			free(buf);
			return -1;
		}

		FILE* stream_dst = fopen(data_dst, "wb");
		if (!stream_dst) {
			std::cout << "open dst data file error" << std::endl;
			std::cout << "data decrypt fail" << std::endl;
			fclose(stream_src);
			free(buf);
			free(decrypted_file);
			return -1;
		}

		fwrite(decrypted_file, 1, decrypt_len, stream_dst);

		fclose(stream_src);
		fclose(stream_dst);
		free(buf);
		free(decrypted_file);

	}
	else {
		std::cout << "it is not a encrypted data file" << std::endl;
		return -1;
	}

	std::cout << "data decrypt success" << std::endl;

	return 0;
}
