#ifndef PROTECTOR_H
#define PROTECTOR_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>

#define KEY  0x67
#define RSA_KEY_SIZE 256
#define MAX_RSA_SIZE (RSA_KEY_SIZE - 11)
void app_protector_init();
void __app_protect_decipher(char start[], char fill[]);
#define APP_PROTECTOR_CHECKTIME() app_protector_init();
struct _crtfct{
	unsigned char crt[3072];
	int sz;
	unsigned int start_date;
	unsigned int end_date;
	char appid[256];
	char machineid[256];
};

extern const struct _crtfct crtfct;


#ifdef _MSC_VER
	#include <windows.h>
	#define DllExport extern "C" _declspec(dllexport) 	
	#define APP_PROTECTOR_FUNC 
	#define OLD_IMAGE_BASE 0x10000000

	#define NOP1 __asm nop
	#define NOP4 NOP1 NOP1 NOP1 NOP1
	#define NOP16 NOP4 NOP4 NOP4 NOP4 NOP4
	#define NOP64 NOP16 NOP16 NOP16 NOP16
	#define NOP256 NOP64 NOP64 NOP64 NOP64
	#define NOP1024 NOP256 NOP256 NOP256 NOP256
	#define NOP4096 NOP1024 NOP1024 NOP1024 NOP1024
#else 
	#include <sys/mman.h>
	#define DllExport
	#define APP_PROTECTOR_FUNC __attribute__((noinline))
#endif 
 
#ifdef _MSC_VER
	/* 4096 should be large enough for reasonable large code padding */
	#define APP_PROTECTOR_START(name)                 \
	{                                                 \
		__asm push offset __appProtect_##name##_fill  \
		__asm push offset __appProtect_##name##_start \
		__asm call __app_protect_decipher             \
		__asm add esp, 8                              \
		__asm __appProtect_##name##_start:            \
		__asm push eax                                \
		__asm pop eax                                 \
		__asm push ebx                                \
		__asm pop ebx                                 \
	}
	#define APP_PROTECTOR_END(name)         \
	{                                       \
		__asm __appProtect_##name##_fill:   \
		__asm jmp __appProtect_##name##_end \
		NOP4096								\
		__asm __appProtect_##name##_end:    \
	}

EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#else
	#define APP_PROTECTOR_START(name) \
		extern char __appProtect_##name##_start[]; \
		extern char __appProtect_##name##_end[]; \
		extern char __appProtect_##name##_fill[]; \
		__app_protect_decipher(__appProtect_##name##_start, \
			__appProtect_##name##_fill ); \
		asm volatile( \
			"__appProtect_" #name "_start:\n" \
			"pushq %rax\n" \
			"pop %rax\n" \
			"pushq %rbx\n" \
			"pop %rbx\n" \
		);
	/* 4096 should be large enough for reasonable large code padding */
	#define APP_PROTECTOR_END(name) \
		asm volatile("__appProtect_" #name "_fill:\n" \
			"jmp __appProtect_" #name "_end\n" \
			".fill 4096, 1, 0x90\n" \
			"__appProtect_" #name "_end:\n" \
		);
#endif

#endif