#!/bin/bash

set -e

if [[ -z "$1" ]]; then
	echo "Usage: $0 [options] [file]"
	exit 1
fi

if [[ -f "$HOME/.cppbuild/config" ]]; then
	paths=$(cat $HOME/.cppbuild/config | grep -e "^path" | cut -d '=' -f 2 \
		| tr '"' ' ' | xargs echo)
	p=$(echo $paths | tr ' ' ':')
	export PATH=$p:$PATH
fi

sign=`file "$1" | cut -d ':' -f 2`
if `echo "$sign" | grep -q 'ARM'`; then
	if `echo "$sign" | grep -q 'ELF 64-bit'`; then
		type="aarch64"
	else
		type="armv7"
	fi
elif `echo "$sign" | grep -q 'x86-64'`; then
	type="x86_64"
elif `echo "$sign" | grep -q 'Intel 80'`; then
	type="x86"
elif `echo "$sign" | grep -q 'ELF 64-bit'`; then
	# XXX ubuntu 12.04 & centos 6 can not recognize aarch64
	echo "warning: your 'file' is too old, assuming aarch64"
	type="aarch64"
else
	echo "Unsupported file format: $sign"
	exit 1
fi

case $type in
	x86_64 | x86)
		prefix=""
		;;
	armv7)
		if `command -v arm-linux-gnueabihf-strip >/dev/null`; then
			prefix="arm-linux-gnueabihf-"
		elif `command -v arm-linux-androideabi-strip >/dev/null`; then
			prefix="arm-linux-androideabi-"
		else
			echo "toolchain for armv7 not found"
			exit 1
		fi
		;;
	aarch64)
		if `command -v aarch64-linux-gnu-strip >/dev/null`; then
			prefix="aarch64-linux-gnu-"
		elif `command -v aarch64-linux-android-strip >/dev/null`; then
			prefix="aarch64-linux-android-"
		else
			echo "toolchain for aarch64 not found"
			exit 1
		fi
		;;
esac

echo "toolchain prefix: $prefix" >&2
"$prefix"strip "$1"

