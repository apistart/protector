Linux 平台下的测试代码

protector: 用于生成 .so 动态链接库
test.c： 测试代码的主程序

用法：
1. 将 install/bin 下面的的 libprotector.a 静态链接库以及 install/include下的 protector.h 头文件拷贝在此目录
2. $ make
3. 未加密运行：
   $LD_LIBRARY_PATH=. ./test
   $76
4. 加密： $ ruby ../../encrpyt/linux/app_protect.rb protector.so 
5. 加密后运行：
   $LD_LIBRARY_PATH=. ./test
   $76
