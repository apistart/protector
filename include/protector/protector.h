#ifndef PROTECTOR_H
#define PROTECTOR_H

#define RSA_KEY_SIZE 256
#define MAX_RSA_SIZE (RSA_KEY_SIZE - 11)

#define APP_MAX_TAG_LEN 64
struct _crtfct{
	unsigned char crt[3072];
	int sz;
	unsigned int end_date;
	unsigned int start_date;
	unsigned char appid[256];
	unsigned char machineid[256];
	unsigned char tag[APP_MAX_TAG_LEN];
};


#ifdef _MSC_VER
	#define APP_PROTECTOR_FUNC __declspec(noinline)
#else
	#define APP_PROTECTOR_FUNC __attribute__((noinline))
	#define APP_PROTECTOR_CHECKTIME() app_protector_check()
#endif

#if (defined _MSC_VER) && !(defined _M_X64)
	/* 4096 should be large enough for reasonable large code padding */
	#define NOP1 __asm nop
	#define NOP4 NOP1 NOP1 NOP1 NOP1
	#define NOP16 NOP4 NOP4 NOP4 NOP4 NOP4
	#define NOP64 NOP16 NOP16 NOP16 NOP16
	#define NOP256 NOP64 NOP64 NOP64 NOP64
	#define NOP1024 NOP256 NOP256 NOP256 NOP256
	#define NOP4096 NOP1024 NOP1024 NOP1024 NOP1024

	#define APP_PROTECTOR_CHECKTIME() app_protector_check()
	#define APP_PROTECTOR_START(name)                \
	{                                                 \
		__asm push offset __appProtect_##name##_fill  \
		__asm push offset __appProtect_##name##_start \
		__asm call __app_protect_decipher             \
		__asm add esp, 8                              \
		__asm __appProtect_##name##_start:            \
		__asm push eax                                \
		__asm pop eax                                 \
		__asm push ebx                                \
		__asm pop ebx                                 \
	}
	/* check encrypt_dll.py if modified! */
	#define APP_PROTECTOR_END(name)         \
	{                                       \
		__asm jmp __appProtect_##name##_end \
		__asm __appProtect_##name##_fill:   \
		NOP1024				    \
		__asm __appProtect_##name##_end:    \
	}
#elif (defined _MSC_VER) && (defined _M_X64)
	#include <intrin.h>
	#define NOP1 __nop();
	#define NOP4 NOP1 NOP1 NOP1 NOP1
	#define NOP16 NOP4 NOP4 NOP4 NOP4 NOP4
	#define NOP64 NOP16 NOP16 NOP16 NOP16
	#define NOP256 NOP64 NOP64 NOP64 NOP64
	#define NOP1024 NOP256 NOP256 NOP256 NOP256


	/* some magic code implemented with intrinsic
	 * call _app_protect_decipher to avoid optimization,
	 * the argument 0, 0 will be modified by encryption script
	 * [nop64]                 # filled with jump code
	 * f0 83 0c 24 00          lock orl $0x0,(%rsp)
	 * 90                      nop
	 * f0 83 0c 24 00          lock orl $0x0,(%rsp)
	 */
	#define APP_PROTECTOR_START(name)      \
		{ \
		_ReadWriteBarrier();            \
		NOP64 \
		_ReadWriteBarrier();            \
		__app_protect_decipher(0, 0);  \
		__appProtect_##name##_start: 	\
		__faststorefence();          \
		__nop(); \
		 __faststorefence();          \
		}
	/* reserve 5 byte for jump buffer,
	 * NOPs will be optimizied out if using goto
	 * e9 00 04 00 00   jmp $+0x400
	 * _ReadWriteBarrier() to prevent compiler from reordering
	 * instructions...
	 */
	#define APP_PROTECTOR_END(name)         \
	{ 					\
		_ReadWriteBarrier();            \
		NOP4 NOP1 			\
		__appProtect_##name##_fill: 	\
		NOP1024				\
		__appProtect_##name##_end: 	\
		_ReadWriteBarrier();            \
	}
#define APP_PROTECTOR_CHECKTIME() app_protector_check()
#elif __arm__ && __ANDROID__
	#define APP_PROTECTOR_START(name) \
		extern char __appProtect_##name##_start[]; \
		extern char __appProtect_##name##_end[]; \
		extern  char __appProtect_##name##_fill[]; \
		__app_protect_decipher(__appProtect_##name##_start, \
			__appProtect_##name##_fill ); \
		asm volatile( \
			"nop\n" \
			"nop\n" \
			"nop\n" \
			"nop\n" \
			".global __appProtect_" #name "_start\n" \
			"__appProtect_" #name "_start:\n" \
			"mov r0, r0\n" \
			"mov r0, r0\n" \
		);
	/* 4096 should be large enough for reasonable large code padding */
	#define APP_PROTECTOR_END(name)         \
		asm volatile( \
			".global __appProtect_" #name "_fill\n" \
			".global __appProtect_" #name "_end\n" \
			"b __appProtect_" #name "_end\n" \
			"__appProtect_" #name "_fill:\n" \
			".fill 256, 4, 0xe1a00000\n"  \
			"__appProtect_" #name "_end:\n" \
		);
#elif __aarch64__ && __ANDROID__
	#define APP_PROTECTOR_START(name) \
		extern char __appProtect_##name##_start[]; \
		extern char __appProtect_##name##_end[]; \
		extern  char __appProtect_##name##_fill[]; \
		__app_protect_decipher(__appProtect_##name##_start, \
			__appProtect_##name##_fill ); \
		asm volatile( \
			"nop\n" \
			"nop\n" \
			"nop\n" \
			"nop\n" \
			".global __appProtect_" #name "_start\n" \
			"__appProtect_" #name "_start:\n" \
			"nop\n" \
			"nop\n" \
		);
	/* 4096 should be large enough for reasonable large code padding */
	#define APP_PROTECTOR_END(name)         \
		asm volatile( \
			".global __appProtect_" #name "_fill\n" \
			".global __appProtect_" #name "_end\n" \
			"b __appProtect_" #name "_end\n" \
			"__appProtect_" #name "_fill:\n" \
			".fill 256, 4, 0xd503201f\n"  \
			"__appProtect_" #name "_end:\n" \
		);


#elif __x86_64__ && __linux__
	#define APP_PROTECTOR_START(name) \
		extern char __appProtect_##name##_start[]; \
		extern char __appProtect_##name##_end[]; \
		extern char __appProtect_##name##_fill[]; \
		__app_protect_decipher(__appProtect_##name##_start, \
			__appProtect_##name##_fill ); \
		asm volatile( \
			"__appProtect_" #name "_start:\n" \
			"pushq %rax\n" \
			"pop %rax\n" \
			"pushq %rbx\n" \
			"pop %rbx\n" \
		);
	/* 4096 should be large enough for reasonable large code padding */
	#define APP_PROTECTOR_END(name) \
		asm volatile(\
			"jmp __appProtect_" #name "_end\n" \
			"__appProtect_" #name "_fill:\n" \
			".fill 1024, 1, 0x90\n" \
			"__appProtect_" #name "_end:\n" \
		); \
		(void)__appProtect_##name##_end;  /* suppress unused warning */
#elif defined(__APPLE__) && defined(__MACH__)
#define APP_PROTECTOR_START(name)
#define APP_PROTECTOR_END(name)
#define APP_PROTECTOR_CHECKTIME() app_protector_check()
#else
#ifdef _MSC_VER
/* stupid mscv...no #warning */
#pragma message ("Warning : protector not supported on this MSVC platform")
#else
#warning "Warning: protector not supported on this architecture"
#endif
#define APP_PROTECTOR_START(name)
#define APP_PROTECTOR_END(name)
#define APP_PROTECTOR_CHECKTIME() 1
#endif

#if CONFIG_PROTECTOR_WITH_HASP
#	define CV_PROTECTOR_CHECKIN(handle, feature) app_protector_checkin(feature, &(handle))
#	define CV_PROTECTOR_CHECKOUT(handle) app_protector_checkout(handle)
#else
#	define CV_PROTECTOR_CHECKIN(handle, feature)	APP_PROTECTOR_CHECKTIME()
#	define CV_PROTECTOR_CHECKOUT(handle)
#endif

#ifdef __cplusplus
extern "C" {
#endif

#  if defined(_MSC_VER)
	typedef unsigned long checkin_handle_t;
	typedef unsigned long checkin_feature_t;
	typedef unsigned long buffer_filed_t;
	typedef unsigned long buffer_size_t;
#  else
	typedef unsigned int checkin_handle_t;
	typedef unsigned long checkin_feature_t;
	typedef unsigned long buffer_filed_t;
	typedef unsigned long buffer_size_t;
#  endif

int app_protector_check();
int app_protector_checkin(checkin_feature_t feature_id, checkin_handle_t *handle);
int app_protector_checkout(checkin_handle_t handle);
#if CONFIG_PROTECTOR_WITH_HASP
int app_protector_read_int32(checkin_handle_t handle, buffer_filed_t filed, buffer_size_t offset);
#endif
void __app_protect_decipher(char start[], char fill[]);
void app_protector_init();
void app_protector_uninit();

void app_protector_send_init_log(const char *module, const char *version,
	const char *channel);

int STEF_is_encrypt_file(const char *fn);

int STEF_is_encrypt_memory(const unsigned char *mem);

int STEF_get_original_size(const unsigned char *mem);

// int file2mem(const char *filename, unsigned char **buf);

int STEF_decrypt2mem(unsigned char *start, unsigned char *fill,
		unsigned char **output_buf);

int STEF_encrypt2mem(unsigned char *start, unsigned char *fill,
		unsigned char **output_buf);


#ifdef __cplusplus
}
#endif

#endif

