#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SYMM_SWAP(ch) (					\
	((ch & 1) << 7)         |			\
	((ch & (2)) << 5)       |			\
	((ch & (4)) << 3)       |			\
	((ch & (8)) << 1)       |			\
	((ch & (16)) >> 1)      |			\
	((ch & (32)) >> 3)      |			\
	((ch & (64)) >> 5)      |			\
	((ch & (128)) >> 7))

#define KEY  0x67

int main()
{
	int i;
	FILE *f = fopen("test_rsa", "rb");
	if(!f)
		return 1;
	fseek(f, 0, SEEK_END);
	long len = ftell(f);
	rewind(f);

	char *text = (char *)malloc(len+1);
	text[len] = 0;
	fread(text, 1, len, f);
	fclose(f);

	printf("before encrypt: %s\n", text);

	len = strlen(text);
	for(i = 0; i < len; i++)
	{
		text[i] = (SYMM_SWAP(text[i]) ^ KEY);
	}

	printf("after encrypt: %s\n", text);

	f = fopen("test_rsa_encrypted", "wb");
	fwrite(text,strlen(text), 1, f);
	fclose(f);

	for(i = 0; i < len; i++)
	{
		text[i] ^= KEY;
		text[i] = SYMM_SWAP(text[i]);
	}

	printf("after decrypt: %s\n", text);

	return 0;
}
