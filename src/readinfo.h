#ifndef SRC_READINFO_H_
#define SRC_READINFO_H_

#define MAX_INFO_LENGTH 512

#ifdef __cplusplus
extern "C" {
#endif

void get_os_type(char *val);

#if __ANDROID__
int property_get(const char *key, char *value, const char *default_value);
#endif

#if __linux__
int linux_get_mac(char *val);
int linux_get_app_id(char *val);
#endif

#if defined(__APPLE__) && defined(__MACH__)
int mach_get_app_id(char *val);
int mach_get_device_id(char *val);
#endif

#ifdef __cplusplus
}
#endif

#endif  // SRC_READINFO_H_
