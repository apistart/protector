#include <TargetConditionals.h>
#import <CoreFoundation/CFBundle.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#import <Foundation/NSBundle.h>
#import <Foundation/NSProcessInfo.h>

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
#import <UIKit/UIDevice.h>
#endif

#include <string.h>
#include "readinfo.h"

int mach_get_app_id(char *val)
{
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
	NSString *name = [[NSBundle mainBundle] bundleIdentifier];
	const char *p = [name UTF8String];

	strncpy(val, p, MAX_INFO_LENGTH);
	val[MAX_INFO_LENGTH-1] = 0;
#else
	val[0] = '\0';
#endif

	return strlen(val);
}

int mach_get_device_id(char *val)
{
	/* only works in iOS >=6.0 */
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
	NSString *udid = [UIDevice currentDevice].identifierForVendor.UUIDString;
	const char *p = [udid UTF8String];

	strncpy(val, p, MAX_INFO_LENGTH);
	val[MAX_INFO_LENGTH-1] = 0;
#else
	val[0] = '\0';
#endif

	return strlen(val);
}
