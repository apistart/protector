#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" encrypt_dll is to encrypt dll using RSA
   this program can encrypt a dll from start_rva to end_rva which has special symbols at the beginning and ending
   usage:
        python encrypt_dll.py ***.dll 20141108 20141115


   Copyright (c) 2014 SenseTime All rights reserved.
"""

__author__ = 'Qingfu Wen, Yuheng Chen'
__version__ = '0.0.1'
__contact__ = 'thssvince@163.com, chyh1990@gmail.com'

from optparse import OptionParser
import pefile
import rsa
import os
import stat
import binascii
import string
import sys
import re
import struct
import platform
reload(sys)
sys.setdefaultencoding("utf-8")


WORK_DIR = os.path.dirname(__file__)

DIADUMP = os.path.join(WORK_DIR, 'Dia2Dump.exe')

# path of RSA public key
public_key_path = os.path.join(WORK_DIR, 'keys', 'test_rsa.pem')
# path of encrypted RSA private key
encrypted_private_key_path = os.path.join(WORK_DIR, 'keys', 'test_rsa_encrypted')
# path of RSA private key
private_key_path = os.path.join(WORK_DIR, 'keys', 'test_rsa')

SET_PRIVATE_KEY = 0     # 1: set private key  0 not set private key
START_DATE = 19700101
END_DATE =  99991231
TAG = "general"
TIME_OFFSET = 3072 + 4
TAG_OFFSET = 3072 + 4 * 3 + 256 * 2
KEY = 0x67       # symmetric encryption key

KEY_SIZE = 256
MAX_RSA_SIZE = KEY_SIZE - 11

IS_X64 = False

def symm_swap(ch):
    return ( ((ch & 1) << 7)|((ch & (2)) << 5)|((ch & (4)) << 3)|((ch & (8)) << 1)
        |((ch & (16)) >> 1)|((ch & (32)) >> 3)|((ch & (64)) >> 5)|((ch & (128)) >> 7))

def symm_encrypt(data, n_byte=4):
    ''' encrypt data by symmetric encryption

        data: the original data, an integer
        n_byte: bytes of the data
        return: the encrypted data
    '''
    value = 0
    for i in xrange(0, n_byte):
        d = data / (1<<(8*(n_byte-1-i)))
        data = data % (1<<(8*(n_byte-1-i)))
        d = symm_swap(d)
        d ^= KEY
        value = (value << 8) + d
    return value

def encrypt(pe, start_rva, fill_rva, end_rva):
    """ encrypt an PE file from start_rva to end_rva

    pe: input parsed PE file
    return: PE file
    """

    tot_size = end_rva - start_rva
    size = (fill_rva - start_rva + 16 + MAX_RSA_SIZE - 1) / MAX_RSA_SIZE * MAX_RSA_SIZE

    key_data = open(public_key_path).read()
    public_key = rsa.PublicKey.load_pkcs1_openssl_pem(key_data)
    private_key_data = open(private_key_path).read()
    private_key = rsa.PrivateKey.load_pkcs1(private_key_data)
    public_key.d = public_key.e
    private_key.e = private_key.d

    cipher = ""
    plaintext = ""
    start = start_rva
    while start < start_rva + size:
        origin = pe.get_data(start, MAX_RSA_SIZE)
        plaintext += origin
        #cipher += rsa.encrypt(origin, public_key)
        cipher += rsa.encrypt(origin, private_key)
        start += MAX_RSA_SIZE

    if tot_size < len(cipher):
        print "cipher too large"
        exit(-1)
    if not pe.set_bytes_at_rva(start_rva, cipher):
        print "set bytes data false"
        exit(-1)
    return pe

def set_private_key(pe, crt_sym):
    """ set the private key

    return: encrypted PE file
    """
    private_key_data = open(encrypted_private_key_path).read()
    pe.set_bytes_at_rva(crt_sym[1] ,private_key_data)
    return pe

def set_date(pe, crt_sym):
    """ set the start_date and end_date

    return: encrypted PE file
    """
    pe.set_dword_at_rva(crt_sym[1]+TIME_OFFSET+4, symm_encrypt(START_DATE))
    pe.set_dword_at_rva(crt_sym[1]+TIME_OFFSET, symm_encrypt(END_DATE))
    return pe

def set_tag(pe, crt_sym):
    """ set tag

    return: encrypted PE file
    """
    pe.set_bytes_at_rva(crt_sym[1]+TAG_OFFSET, TAG)
    return pe

def parse_encrypt(pe, segs):
    """ find start_rva and end_rva of symbols, encrypt them

    return: encrypted PE file
    """
    start_rva = []
    fill_rva = []
    end_rva = []
    count = len(segs)

    for x in segs:
        start_rva.append(x[1])
        fill_rva.append(x[2])
        end_rva.append(x[3])


    if IS_X64:
        magic = 'f0830c24'
    else:
        magic = '5058535b'
    for i in xrange(0, count):
        if binascii.hexlify(pe.get_data(start_rva[i],4)) == magic:
            print >> sys.stderr, "Patching %s" % segs[i][0]
            pe = encrypt(pe, start_rva[i], fill_rva[i], end_rva[i])
            #print "HERE"
            pass
        else:
            print "already encrypted"

    return pe


def search_exports(pe):
    print "Export table:"
    for exp in pe.DIRECTORY_ENTRY_EXPORT.symbols:
        print hex(pe.OPTIONAL_HEADER.ImageBase + exp.address), exp.name, exp.ordinal
    sym_ranges = []
    # sort symbols
    syms = sorted(pe.DIRECTORY_ENTRY_EXPORT.symbols, key=lambda x: x.address)
    max_search_len = 64 * 4096
    # XXX last one acts as the boundary...
    for i in xrange(len(syms) - 1):
	    sym_ranges.append( (syms[i].name, syms[i].address,
			    syms[i+1].address, syms[i]) )
    print sym_ranges
    if IS_X64:
        pattern = '\xf0\x83\x0c\x24\x00'  #lock orl $0x0,(%rsp)
        pattern += '\x90' # nop
        pattern += '\xf0\x83\x0c\x24\x00'  #lock orl $0x0,(%rsp)
    else:
        pattern = '\x83\xc4\x08'  # add exp, 8
        pattern += '\x50\x58\x53\x5b' # push eax; pop eax; push ebx; pop ebx

    pattern_end = '\x90' * 128

    found_segs = []
    for exp in sym_ranges:
        ep = exp[1]
# XXX wrong size
	func_size = exp[2] - exp[1]
	assert func_size >= 0
        search_size = min(func_size, max_search_len)
	print >> sys.stderr, "Searching %s: 0x%x, %d" % (exp[0], ep, search_size)
        data = pe.get_memory_mapped_image()[ep:ep + search_size]
	#print map(hex, map(ord, data))
        offset = data.find(pattern)
	#print offset
	if offset < 0:
		continue
        if IS_X64:
	    va_start = ep + offset
        else:
	    va_start = ep + offset + 3 # remove add exp, 8
        offset = data.find(pattern_end)
	if offset < 0:
		continue
	va_fill = ep + offset
        if IS_X64:
            va_fill += 5 # for jmp buffer
        # check
	if va_fill < va_start:
            print >> sys.std, "Warning: in %s, va_fill < va_start, bad binary?" % exp[0]
	    continue
        # hard coded
        fill_size = 1024
        va_end = va_fill + fill_size
        found_segs.append( (exp[0], va_start, va_fill, va_end) )
    print "Found %d exports:" % len(found_segs)
    for x in found_segs:
        print "\t%s: 0x%x, 0x%x, 0x%x" %x
    return found_segs

def find_public_symbol(pe, symname):
    if 'CYGWIN' in platform.system():
        winpath = os.popen('cygpath -w %s' % (PE_filePath)).read().rstrip()
    else:
        winpath = PE_filePath
    cmd = DIADUMP + " -p '%s'" % (winpath)
    print cmd
    pip = os.popen(cmd)
    raw_symbol = ""
    for line in pip.readlines():
        if symname in line:
            raw_symbol = line.rstrip()
            break
    pip.close()
    #print raw_symbol
    if len(raw_symbol) < 8:
        print >> sys.stderr, 'Symbol not found by Dia2Dump, no PDB?'
        sys.exit(1)
    p = '\[([0-9A-F]+)\]\[(.+)\]'
    r = re.search(p, raw_symbol)
    if not r:
        print >> sys.stderr, 'fail to parse: "%s"' % raw_symbol
        sys.exit(1)

    return (symname, string.atoi(r.group(1), base=16))

def write_x64_inst(pe, segs):
    for seg in segs:
        #jmp $+0x400
        pe.set_bytes_at_rva(seg[2] - 5, '\xe9\x00\x04\x00\x00')

        # hopefully, it's
        # xor    %edx,%edx
        # xor    %edx,%ecx
        # call xxx
        call_buf_offset = seg[1] - 64
        #pe.set_bytes_at_rva(call_buf_offset, '\xcc')
#   0:   48 8d 0d f9 ff ff ff    lea    -0x7(%rip),%rcx        # 0x0
#   7:   48 83 c1 40             add    $0x40,%rcx
#   b:   48 ba fe fe fe fe 00    movabs $0xfefefefe,%rdx
#  12:   00 00 00
#  15:   48 01 ca                add    %rcx,%rdx

        code_size = seg[2] - seg[1]
        patch = ''
        patch += '\x48\x8d\x0d\xf9\xff\xff\xff'
        patch += '\x48\x83\xc1\x40'
        patch += '\x48\xba\xfe\xfe\xfe\xfe\x00'
        patch += '\x00\x00\x00'
        patch += '\x48\x01\xca'
        patch = patch.replace('\xfe\xfe\xfe\xfe', struct.pack('I', code_size))
        pe.set_bytes_at_rva(call_buf_offset, patch)

        call_size = 5 + 2 + 2
        xor_off = seg[1] - call_size
        orig = pe.get_data(xor_off, 4)
        #print map(hex, map(ord, orig))
        # hopefully, it's
        # xor    %edx,%edx
        # xor    %edx,%ecx
        assert (orig == '\x33\xd2\x33\xc9') or (orig == '\x33\xc9\x33\xd2')
        # NOP it
        pe.set_bytes_at_rva(xor_off, '\x90\x90\x90\x90')
        #pe.set_bytes_at_rva(xor_off, '\xcc')

if __name__ == "__main__":
	usage = "Usage: ./encrypt_dll.py [options] file"
	parser = OptionParser(usage=usage)
	parser.add_option("-t", "--timelimit",type="string",
		action="store", dest="time", default="19700101,20991231",
		help="Set timelimit to *.dll (Example:19700101,20991231)")


	parser.add_option("-g", "--tag",type="string",
		action="store", dest="tag", default="general",
		help="Set tag to *.dll (Example:Sensetime_test_20000101)")


	parser.add_option("-e", "--encrypt",
		action="store_true", dest="encrypt",default=False,
		help="Encrypt the *.dll")

	parser.add_option("-o", "--overwrite",
		action="store_true", dest="overwrite",default=False,
		help="Overwrite the file")

	(options, args) = parser.parse_args()
	timelimit = options.time.split(',')
	try:
		if len(timelimit) > 1:
			START_DATE = int(timelimit[0])
			END_DATE = int(timelimit[1])
		elif len(timelimit) == 1:
			END_DATE = int(timelimit[0])
	except:
		print "Warning: wrong time type!"

	TAG = options.tag

	print(START_DATE)
	print(END_DATE)
	print(TAG)
	print(args)

	PE_filePath = args[0]
	out_file = args[0] + '.new'

	pe = pefile.PE(PE_filePath)

	print pefile.MACHINE_TYPE[pe.FILE_HEADER.Machine]
	# IMAGE_FILE_MACHINE_AMD64, 0x8664
	if pe.FILE_HEADER.Machine == 0x8664:
		IS_X64 = True

	print >> sys.stderr, "searching symbol crtfct"
	crtfct = find_public_symbol(pe, 'crtfct')

	segs = search_exports(pe)

	print >> sys.stderr, "patching..."

	if IS_X64:
		write_x64_inst(pe, segs)
	#pe.write(out_file)
	#sys.exit()
	if options.encrypt:
		if SET_PRIVATE_KEY:
			pe = set_private_key(pe, crtfct)
	pe = set_date(pe, crtfct)
	pe = set_tag(pe, crtfct)
	if options.encrypt:
		pe = parse_encrypt(pe, segs)

	print >> sys.stderr, "write out %s" % out_file
	pe.write(out_file)
	# fix permission
	os.chmod(out_file, stat.S_IWRITE|stat.S_IREAD|stat.S_IEXEC)
	if options.overwrite:
		os.remove(args[0])
		os.rename(out_file, args[0])

	# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4


