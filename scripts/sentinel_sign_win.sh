#!/bin/bash

set -e

if [[ -z "$TOOLSET_ROOT" ]]; then
	TOOLSET_ROOT="$HOME/toolset/encryption"
fi

ENVELOPE_WIN_PATH="$TOOLSET_ROOT/VendorSuite"

if [[ $# != 3 ]]; then
	echo "Usage: $0 src arch feature"
	exit 1
fi

echo "Toolset root: $TOOLSET_ROOT"

src=$1
arch=$2
feature=$3

sentinel_env=$ENVELOPE_WIN_PATH/envelope.com
template=$ENVELOPE_WIN_PATH/autobuild.prjx.template
input=$(cygpath -m -a $1)
output="$input".protected
proj=$(mktemp /tmp/dongle_XXXX.prjx)

# workaround NFS
tmp_hvc=/tmp/QQWIE.hvc
cp "$TOOLSET_ROOT/VendorCodes/QQWIE.hvc" $tmp_hvc
win_tmp_hvc=$(cygpath -m $tmp_hvc)

echo "Encrypting with sentinel HL..."
cat $template | sed -e "s,__ARCH__,$arch,g" -e "s,__DLL_NAME__,$(basename $1),g" \
	-e "s@__INPUT_FILE__@$input@g" -e "s@__OUTPUT_FILE__@$output@g" \
	-e "s@__FEATURE_ID__@$feature@g" \
	-e "s@__TOOLSET_ROOT__@$win_tmp_hvc@g" > "$proj"
# cat $proj
$sentinel_env -p $(cygpath -m -a $proj)

rm -f $tmp_hvc

mv "$1".protected "$1"
rm -f "$proj"
echo "sentinel Done"

