#ifndef PROTECTOR_H
#define PROTECTOR_H
#include <stdio.h>
#define SYMMETRIC_KEY  0x67
#define RSA_KEY_SIZE 256
#define MAX_RSA_SIZE (RSA_KEY_SIZE - 11)
extern "C" void app_protector_check();
extern "C" void __app_protect_decipher(char start[], char fill[]);
struct _crtfct{
	unsigned char crt[3072];
	int sz;
	unsigned int end_date;
	unsigned int start_date;
	char appid[256];
	char machineid[256];
};

extern const struct _crtfct crtfct;
extern "C" int send_app_id(char *id);

#define DPRINTF(fmt, ...) __android_log_print(ANDROID_LOG_INFO, "PROTECTOR", fmt, ##__VA_ARGS__) 
#ifdef _MSC_VER
	#include <windows.h>
	#define DllExport extern "C" _declspec(dllexport) 	
	#define APP_PROTECTOR_FUNC 
	#define OLD_IMAGE_BASE 0x10000000
	#define APP_PROTECTOR_CHECKTIME() //app_protector_check();
	
	#define NOP1 __asm nop
	#define NOP4 NOP1 NOP1 NOP1 NOP1
	#define NOP16 NOP4 NOP4 NOP4 NOP4 NOP4
	#define NOP64 NOP16 NOP16 NOP16 NOP16
	#define NOP256 NOP64 NOP64 NOP64 NOP64
	#define NOP1024 NOP256 NOP256 NOP256 NOP256
	#define NOP4096 NOP1024 NOP1024 NOP1024 NOP1024
#else 
	#include <sys/mman.h>
	#define DllExport
	#define APP_PROTECTOR_FUNC __attribute__((noinline))
	#define APP_PROTECTOR_CHECKTIME() app_protector_check();
#endif 
 
#ifdef _MSC_VER
	/* 4096 should be large enough for reasonable large code padding */
	#define APP_PROTECTOR_START(name) //                \
	{                                                 \
		__asm push offset __appProtect_##name##_fill  \
		__asm push offset __appProtect_##name##_start \
		__asm call __app_protect_decipher             \
		__asm add esp, 8                              \
		__asm __appProtect_##name##_start:            \
		__asm push eax                                \
		__asm pop eax                                 \
		__asm push ebx                                \
		__asm pop ebx                                 \
	}
	#define APP_PROTECTOR_END(name)//         \
	{                                       \
		__asm jmp __appProtect_##name##_end \
		__asm __appProtect_##name##_fill:   \
		NOP4096								\
		__asm __appProtect_##name##_end:    \
	}

EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#elif __arm__
	#define APP_PROTECTOR_START(name) \
		extern char __appProtect_##name##_start[]; \
		extern char __appProtect_##name##_end[]; \
		extern  char __appProtect_##name##_fill[]; \
		__app_protect_decipher(__appProtect_##name##_start, \
			__appProtect_##name##_fill ); \
		asm volatile( \
			"nop\n" \
			"nop\n" \
			"nop\n" \
			"nop\n" \
			".global __appProtect_" #name "_start\n" \
			"__appProtect_" #name "_start:\n" \
			"nop\n" \
		);
	/* 4096 should be large enough for reasonable large code padding */
	#define APP_PROTECTOR_END(name)         \
		asm volatile( \
			".global __appProtect_" #name "_fill\n" \
			".global __appProtect_" #name "_end\n" \
			"b __appProtect_" #name "_end\n" \
			"__appProtect_" #name "_fill:\n" \
			".fill 256, 4, 0xe1a00000\n"  \
			"__appProtect_" #name "_end:\n" \
		);

#else
	#define APP_PROTECTOR_START(name) \
		extern char __appProtect_##name##_start[]; \
		extern char __appProtect_##name##_end[]; \
		extern char __appProtect_##name##_fill[]; \
		__app_protect_decipher(__appProtect_##name##_start, \
			__appProtect_##name##_fill ); \
		asm volatile( \
			"__appProtect_" #name "_start:\n" \
			"pushq %rax\n" \
			"pop %rax\n" \
			"pushq %rbx\n" \
			"pop %rbx\n" \
		);
	/* 4096 should be large enough for reasonable large code padding */
	#define APP_PROTECTOR_END(name) \
		asm volatile(\
			"jmp __appProtect_" #name "_end\n" \
			"__appProtect_" #name "_fill:\n" \
			".fill 1024, 1, 0x90\n" \
			"__appProtect_" #name "_end:\n" \
		);
#endif

#endif
