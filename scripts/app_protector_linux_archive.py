#!/usr/bin/env python
# -*- coding: utf-8 -*-
from optparse import OptionParser
from subprocess import call
from subprocess import Popen, PIPE
import os
import sys
import stat
import tempfile
import shutil
import ntpath
import struct

reload(sys)
sys.setdefaultencoding("utf-8")
START_DATE = 19700101
END_DATE =  99991231
TIME_OFFSET = 3072 + 4
KEY = 0x67       # symmetric encryption key

def symm_swap(ch):
	return (((ch & 1) << 7)|((ch & (2)) << 5)|((ch & (4)) << 3)|((ch & (8)) << 1)
		|((ch & (16)) >> 1)|((ch & (32)) >> 3)|((ch & (64)) >> 5)|((ch & (128)) >> 7))

def symm_encrypt(data, n_byte=4):
	value = 0
	for i in xrange(0, n_byte):
		d = data / (1<<(8*(n_byte-1-i)))
		data = data % (1<<(8*(n_byte-1-i)))
		d = symm_swap(d)
		d ^= KEY
		value = (value << 8) + d
	return value

def set_date(protector_obj_path, crtfct_offset):
	start_date_ll = struct.pack('l', symm_encrypt(START_DATE))
	end_date_ll = struct.pack('l', symm_encrypt(END_DATE))
	print START_DATE,  start_date_ll, END_DATE, end_date_ll
	with open(protector_obj_path, 'r+b') as f:
		f.seek(crtfct_offset + TIME_OFFSET)
		f.write(end_date_ll)
		f.seek(crtfct_offset + TIME_OFFSET + 4)
		f.write(start_date_ll)


def find_crtfct_offset(protector_obj):
	# nm/objdump/readelf all failed on linux to locate the symbol
	#p = Popen('nm ' + protector_obj + ' | grep crtfct', shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	#output, err = p.communicate()
	#print >> sys.stderr, "locate symbol crtfct offset", output, err
	#info = output.split()
	#if not (info[6]=='crtfct'):
	#	print >> sys.stderr, "symbol crtfct not defined"
	#	quit()
	#crtfct_offset = int(info[0], 16)
	CRT_MAGIC = '\xd3\xd3\xd3\xd3'
	with open('protector.cpp.o', mode='rb') as f:
		file_content = f.read()
		crtfct_offset = file_content.find(CRT_MAGIC)
	return crtfct_offset


if __name__ == "__main__":
	usage = "Usage: ./app_protector_linux_archive_set_date.py [options] file/path/to/libxxx.a"
	parser = OptionParser(usage=usage)
	parser.add_option("-t", "--timelimit",type="string",
		action="store", dest="time", default="19700101,20991231",
		help="Set timelimit to *.dll (Example:19700101,20991231)")
	parser.add_option("-o", "--overwrite",
		action="store_true", dest="overwrite",default=False,
		help="Overwrite the file")
	(options, args) = parser.parse_args()
	if len(args) == 0:
		print >> sys.stderr, "not input archive file"
		quit()

	timelimit = options.time.split(',')
	try:
		if len(timelimit) > 1:
			START_DATE = int(timelimit[0])
			END_DATE = int(timelimit[1])
		elif len(timelimit) == 1:
			END_DATE = int(timelimit[0])
	except:
		print "Warning: wrong time type!"

	print(START_DATE)
	print(END_DATE)

	archive_file_path = os.path.abspath(args[0])
	if not os.path.isfile(archive_file_path):
		print >> sys.stderr, "archive file not exists."
		quit()

	archive_file_name = ntpath.basename(archive_file_path)
	out_file_path = args[0] + '.new'

	temp_dir = tempfile.mkdtemp()
	print >> sys.stderr, "make tmp dir:", temp_dir
	os.chdir(temp_dir)
	print archive_file_path, archive_file_name
	shutil.copyfile(archive_file_path, temp_dir + '/' + archive_file_name)
	call(['ar', 'xv', archive_file_name])
	os.remove(archive_file_name)

	protector_obj = 'protector.cpp.o'
	if not os.path.isfile(protector_obj):
		print >> sys.stderr, "protector.cpp's object file to modify not exists."
		quit()

	p = Popen(['file', protector_obj], stdin=PIPE, stdout=PIPE, stderr=PIPE)
	output, err = p.communicate()
	print >> sys.stderr, "checking file type", output, err
	info = output.split()
	if not (info[1]=='ELF' and info[2]=='64-bit'):
		print >> sys.stderr, "file format not supported"
		quit()

	crtfct_offset = find_crtfct_offset(protector_obj)
	print >> sys.stderr, "symbol found at", crtfct_offset

	set_date(protector_obj, crtfct_offset)
	os.system('ar cvq '+' '+archive_file_name+' *.o')

	print >> sys.stderr, "write out %s" % out_file_path

	shutil.copyfile(archive_file_name, out_file_path)

	# fix permission
	os.chmod(out_file_path, stat.S_IWRITE|stat.S_IREAD|stat.S_IEXEC)
	if options.overwrite:
		os.remove(args[0])
		os.rename(out_file_path, args[0])

        shutil.rmtree(temp_dir)

