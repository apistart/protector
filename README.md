# AppPROTECTOR

**AppProtector** can generate static library for encrypting code and managing the SDK's expiration date on Windows and Linux.

一个跨 Windows、 Linux、Android 平台的工具，可以生成一个静态链接库，完成对程序中的代码段进行加密、对程序的使用期限进行管理等功能。

## Contents
-src: source code
-encrypt: scrpits for encrypting dynamic link library
-test: some test examples

## Windows
### Requirements
* VS2012
* Python 2.7
* CMake 2.8+

### Build
$ build.bat


## Linux
### Requirements
* gcc
* Ruby 2.1.2+
* CMake 2.8+

### Build
$ sh build.sh


## Android
### Requirements
* gcc
* Ruby 2.1.2+
* CMake 2.8+
* android-toolchain

### Build
$ sh build\_android.sh
