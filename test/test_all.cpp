#include <gtest/gtest.h>
#include <protector/protector.h>

extern "C" int stest(int x) {
	return x + x;
}

int APP_PROTECTOR_FUNC test(int x)
{
	static int k = 3;
APP_PROTECTOR_START(test)
	x = x+ 2 * k;
	int y = stest(x);
APP_PROTECTOR_END(test)

	return y+1;
}

int APP_PROTECTOR_FUNC test1(int x)
{
	static int k = 3;
APP_PROTECTOR_START(test1)
APP_PROTECTOR_CHECKTIME();
	x = x+ 2 * k;
	int y = stest(x);
APP_PROTECTOR_END(test1)

	return y+1;
}

extern "C" int bar(int x){
	return x+test(x) + test1(x);
}

TEST(Protector, macros) {
	EXPECT_EQ(4, stest(2));
	EXPECT_EQ(36, bar(2));
}

#define _SYMBOL_HIDDEN
extern struct _crtfct crtfct;
#include "../src/st_exchange.h"

TEST(CRT, info_appid) {
	char tmp[256];
	size_t len = strlen((char*)crtfct.appid);
	memcpy(tmp, crtfct.appid, len + 1);
	exchange(tmp, len);
	EXPECT_STREQ(tmp, "sensetimeisagreatcompany");
}

TEST(CRT, info_time) {
	unsigned int t;
	t = crtfct.start_date;
	exchange((char*)&t, 4);
	EXPECT_LE(t, 20000101);

	t = crtfct.end_date;
	exchange((char*)&t, 4);
	EXPECT_EQ(t, 99991231);
}

int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

