#!/usr/bin/env ruby

require 'tmpdir'
require 'fileutils'
require_relative 'protect_common'
require_relative 'smart_ar'

CRT_MAGIC="\xd3\xd3\xd3\xd3".b

def _set_time io, crt_pa, start_date, end_date
  buf = [symm_encrypt(end_date),
    symm_encrypt(start_date)].pack("LL")   # first one is end_time
  crt_pa += TIME_OFFSET
  io.seek crt_pa
  io.write(buf)
end

def _set_tag io, crt_pa, tag
  fail 'tag too long' if tag.size > 60
  crt_pa += TAG_OFFSET
  io.seek crt_pa
  io.write(tag)
end

def _set_appid io, crt_pa, appid
  crt_pa += APPID_OFFSET
  buf = symm_encrypt_str(appid)+[0].pack("C*")
  fail 'appid too long' if buf.size > 255
  io.seek crt_pa
  io.write(buf)
end

def set_info obj, options = {}
  line = `size -l #{obj}`.split("\n").first
  fail 'Not object file' unless line =~ /fileoff\s(\d+)/
  fileoff = $1.to_i
  # search for definition symbol only
  line = `nm #{obj} |grep _crtfct`.split("\n")
    .map{|e| e.split}.select{|e| e[1] == 'D' || e[1] == 'd'}.first
  fail '_crtfct symbol not found' unless line && line[0] =~ /^([0-9a-f]+)/
  sym_off = line[0].to_i(16)
  sym_fileoff = fileoff + sym_off
  STDERR.puts "sym_fileoff: 0x#{sym_fileoff.to_s(16)}"
  File.open obj, "rb+" do |io|
    io.seek sym_fileoff
    magic = io.read(4)
    fail 'invalid offset, magic check failed' unless magic == CRT_MAGIC
    # TODO(chenyh) modify it
    if options[:timelimit]
      _set_time io, sym_fileoff, options[:start_date], options[:end_date]
    end
    if options[:appid]
      _set_appid io, sym_fileoff, options[:appid]
    end
    if options[:tag]
      _set_tag io, sym_fileoff, options[:tag]
    end
  end
end

options = ProtectorOptParser.parse!(ARGV)
input = ARGV[0]
archs = `lipo -info '#{input}'`.strip
fail "NOT Mach-O " unless archs =~ /Architectures/
archs = archs.split(":").last.split

warn 'Warning: encryption not supported on darwin' if options[:encrypted]

# workdir = '/tmp/work00'
# FileUtils.mkdir_p workdir

Dir.mktmpdir do |workdir|
  #workdir = '/tmp/work00'
  outs = []
  archs.each do |arch|
    STDERR.puts arch
    out = "#{workdir}/#{arch}.out"
    `lipo -thin #{arch} -output #{out} #{input}`
    type = `file #{out}`.strip
    if type =~ /Mach-O.*object/
      set_info out, options
      outs << out
    elsif type =~ /current ar archive/
      # ar x first
      unar_dir = File.join workdir, arch
      unpack_ar out, unar_dir
      pro_obj = Dir.glob("#{unar_dir}/*-protector.o").first
      # fall back for partial linked lib
      pro_obj ||= Dir.glob("#{unar_dir}/*-__all.a.o").first
      fail 'protector.o or __all.a.o not found' unless pro_obj
      set_info pro_obj, options
      # repack
      outar = "#{workdir}/#{arch}_e.a"
      `ar rcs #{outar} #{Dir.glob("#{unar_dir}/*.o").join ' '}`
      fail 'ar fail' unless $?.to_i == 0
      outs << outar
    else
      fail "Unsupported file format: #{type}"
    end
  end

  # repack
  tmp = "#{input}.tmp"
  STDERR.puts "Repacking to #{tmp}"
  `lipo -create -output '#{tmp}' #{outs.join ' '}`
  fail 'fail to repack' unless $?.to_i == 0

  FileUtils.mv(tmp, input) if options[:overwrite]
end

