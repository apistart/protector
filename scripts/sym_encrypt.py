# -*- coding: utf-8 -*-
""" symmetric encryption for a string
   usage:
        python sym_encrypt  [string]


   Copyright (c) 2014 SenseTime All rights reserved.
"""

__author__ = 'Qingfu Wen'
__version__ = '0.0.1'
__contact__ = 'thssvince@163.com'


import os
import sys
reload(sys)
sys.setdefaultencoding("utf-8")


KEY = 0x67       # symmetric encryption key


def sym_swap(ch):
    return ( ((ch & 1) << 7)|((ch & (2)) << 5)|((ch & (4)) << 3)|((ch & (8)) << 1)
        |((ch & (16)) >> 1)|((ch & (32)) >> 3)|((ch & (64)) >> 5)|((ch & (128)) >> 7))

def sym_encrypt(st):
    ''' encrypt string by symmetric encryption

        return: the encrypted string
    '''
    s = ""
    length = len(st)
    for i in xrange(0, length):
        d = sym_swap(ord(st[i]))
        d ^= KEY
	s += chr(d)
    return s

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print "one string parameter needed!"
        print "usage:"
	print "      python sym_encrypt.py [ string ]"
        exit(-1)

    st = sym_encrypt(sys.argv[1])
    st = st.encode("hex")
    print st

    f = open("hex.txt",'w')
    length = len(st)

    i = 0
    while i < length:
	s = st[i:i+2]
	f.write("0x%s," % s)
	i += 2
	if i % 30 == 0:
            f.write("\n")
    f.close()


