#include <cassert>
#include <ctime>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <sstream>
#include <vector>
#include "logger.hpp"
#include "protector/protector.h"
#include "readinfo.h"
#include "protector/crypto/crypto.h"
#include "st_exchange.h"

//#define DEBUG_OUTPUT

#ifndef _MSC_VER
#include <sys/mman.h>
#endif

#ifdef __ANDROID__
#include <android/log.h>
#endif

#ifdef __ANDROID__
#define PRINTF(fmt, ...) \
	__android_log_print(ANDROID_LOG_ERROR, "PROTECTOR", fmt, ##__VA_ARGS__)
#else
#define PRINTF(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#endif

#ifdef DEBUG_OUTPUT
#ifdef __ANDROID__
#define DPRINTF(fmt, ...) \
	__android_log_print(ANDROID_LOG_INFO, "PROTECTOR", fmt, ##__VA_ARGS__)
#else
#define DPRINTF(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#endif
#else
#define DPRINTF(fmt, ...)
#endif

#ifdef DEBUG_OUTPUT
#define PROT_ASSERT(x) assert(x)
#else
#define PROT_ASSERT(x)
#endif

#ifdef __arm__
#define MAGIC_CODE 0xe1a00000
#elif __aarch64__
#define MAGIC_CODE 0xd503201f
#else
#if (defined _MSC_VER) && (defined _M_X64)
#define MAGIC_CODE 0x240c83f0
#else
#define MAGIC_CODE 0x5b535850
#endif  /* _MSC_VER */
#endif

#ifdef _MSC_VER
#include <Windows.h>
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
static inline HMODULE GetCurrentModuleHandle() { return (HMODULE)&__ImageBase; }
#endif

#ifndef _MSC_VER
#include <pthread.h>
#else
typedef  HANDLE pthread_mutex_t;
#define pthread_mutex_lock(pobject) WaitForSingleObject(*pobject,INFINITE)
#define pthread_mutex_unlock(pobject) ReleaseMutex(*pobject)
#define pthread_mutex_init(pobject,pattr) (*pobject=CreateMutex(NULL,FALSE,NULL))
#define pthread_mutex_destroy(p) CloseHandle(*p)
#endif

#ifdef _MSC_VER
#define _SYMBOL_HIDDEN
#ifdef _M_X64
#define OLD_IMAGE_BASE 0x0000000180000000ULL
#else
#define OLD_IMAGE_BASE 0x10000000
#endif
#else
#define _SYMBOL_HIDDEN __attribute__((visibility("hidden")))
#endif

#include "crt.h.in"

// read private key
static inline int read_pem(RSA_CTX **rsa_ctx) {
	int len = strlen((const char *)crtfct.crt);
	char *text = (char *)malloc(len + 1);
	memcpy(text, crtfct.crt, len);
	exchange(text, len);
	asn1_read_pem(text, strlen(text), rsa_ctx);
	free(text);
	PROT_ASSERT(*rsa_ctx != NULL);
	// RSA_print(*rsa_ctx);

	return 0;
}

#ifdef _MSC_VER

/*
// only rebased ImageBase is return...
static inline DWORD _get_old_image_base() {
	HANDLE hHandle = GetCurrentModuleHandle();
	PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER)(hHandle); //dos header
	PIMAGE_NT_HEADERS ntHeaders = (PIMAGE_NT_HEADERS)(((const
uint8_t*)(dosHeader)) + dosHeader->e_lfanew);  //
	DWORD base = ntHeaders->OptionalHeader.ImageBase;
	return base;
}
*/
/*
 * @param flag
 *          >1 : relocate before decryption
 *          =0 : relocate after decryption
 */
static inline void fix_relocations(IMAGE_BASE_RELOCATION *base_reloc,
				   DWORD dir_size, ULONG_PTR new_imgbase,
				   char start[], char fill[], short flag) {
	DPRINTF("fix_reloc\n");
	IMAGE_BASE_RELOCATION *cur_reloc = base_reloc, *reloc_end;
	SSIZE_T offset;
	void *start_addr = (void *)((uintptr_t)start & ~0xfff);
	int ret, len = (int)(fill - start);
	DWORD Protect = 0;
	DWORD OldProtect = 0;

	if (flag) {
		offset = new_imgbase - OLD_IMAGE_BASE;
	} else {
		offset = OLD_IMAGE_BASE - new_imgbase;
	}
	DPRINTF("new_imgbase: %p, reloc offset %p\n", (char *)new_imgbase,
		(char *)offset);

	reloc_end = (IMAGE_BASE_RELOCATION *)((char *)base_reloc + dir_size);
	while (cur_reloc < reloc_end && cur_reloc->VirtualAddress) {
		int count = (cur_reloc->SizeOfBlock -
			     sizeof(IMAGE_BASE_RELOCATION)) /
			    sizeof(WORD);
		WORD *cur_entry = (WORD *)(cur_reloc + 1);
		void *page_va = (void *)((char *)new_imgbase +
					 cur_reloc->VirtualAddress);
		while (count--) {
/* is valid x86 relocation? */
#ifdef _M_X64
			bool is_reloc =
				(*cur_entry >> 12) == IMAGE_REL_BASED_DIR64;
#else
			bool is_reloc =
				(*cur_entry >> 12) == IMAGE_REL_BASED_HIGHLOW;
#endif
			size_t addr = (size_t)((char *)page_va +
					       (*cur_entry & 0x0fff));
			if (is_reloc && addr >= (size_t)start &&
			    addr < (size_t)fill) {
				// DPRINTF("addr: 0x%x\n",(DWORD)start);
				DPRINTF("reloc_addr: %p\n", (char *)addr);
				// DPRINTF("offset: 0x%x\n",offset);
				ret = VirtualProtect(start, len,
						     PAGE_EXECUTE_READWRITE,
						     &OldProtect);
				PROT_ASSERT(ret != 0);
				*(size_t *)addr -= offset;
				VirtualProtect(start, len, OldProtect,
					       &Protect);
				PROT_ASSERT(ret != 0);
			}

			cur_entry++;
		}
		/* advance to the next one */
		cur_reloc = (IMAGE_BASE_RELOCATION *)((char *)cur_reloc +
						      cur_reloc->SizeOfBlock);
	}
}
#endif

// copied and modified from rsa.c
bigint *RSA_private_clone(const RSA_CTX *c, bigint *bi_msg) {
	BI_CTX *ctx = c->bi_ctx;
	ctx->mod_offset = BIGINT_M_OFFSET;
	return bi_mod_power(ctx, bi_msg, c->d);
}

// copied and modified from rsa.c
static int RSA_decrypt_clone(const RSA_CTX *ctx, const uint8_t *in_data,
		      uint8_t *out_data, int is_decryption) {
	const int byte_size = ctx->num_octets;
	int i, size;
	bigint *decrypted_bi, *dat_bi;
	uint8_t *block = (uint8_t *)alloca(byte_size);

	memset(out_data, 0, byte_size); /* initialise */

	/* decrypt */
	dat_bi = bi_import(ctx->bi_ctx, in_data, byte_size);

	decrypted_bi = RSA_private_clone(ctx, dat_bi);
	/* convert to a normal block */
	bi_export(ctx->bi_ctx, decrypted_bi, block, byte_size);

	i = 10; /* start at the first possible non-padded byte */

	{
		while (block[i++] && i < byte_size)
			;
	}
	size = byte_size - i;

	/* get only the bit we want */
	if (size > 0) memcpy(out_data, &block[i], size);

	return size ? size : -1;
}

#if defined(__APPLE__) && defined(__MACH__)
extern "C" void sys_icache_invalidate(void *start, size_t len);
#endif

#if CONFIG_PROTECTOR_WITH_HASP
#include "safenet/include/hasp_api.h"
unsigned char vendor_code[] =
	"l6DQZb3HFOaMrRgWUIBXeb16i9845ryKQ35IkxaKbNuwl37Ui4+f/Z45Ym8V5gna6TO6D7M2Sgg84bzc"
	"jNYOhhS80buvqdRxHEWMyTQ3wRtLCmlMyLgitoTBZTMBzEUD6IWYwCFKySGCDEaOn3jujmAX0wcase2s"
	"Oi86uuPSSLEEw6/rD9O3iF+fSOErOnMsQK1/Sr8S07Kp8rYREOm2wZRch01Tfe2ssKEgvVQX2H77afmV"
	"XLsBokrAdh0DkQUiQC4LTRNWPjqkSGoEWFO7s9BMyHL/GVvfm14CGg/29JczPVIdjdJgEXoxdlvV34fs"
	"nKh+xjyUDhjY0wpcutHaK1ec1irLFiFNToEwHWp5IAsVSpKALWsEIuoMcG/W7mFr+9aPFKEvqA/ggvlC"
	"l0JS0++56SmQgOkJnN6VY00Yrkf1aYY4UWL+xjKA3K98EIdmGBrgBh1pBOXtlKEljhls4EVUze3fYNF8"
	"xUtV8nPu7mtvbFBzl4CQSZLuWWxwJtGWrXPqssz/XHFGZjTlwtI9JqAiWcukQIY2fofJ91n5x4oysfA6"
	"l92DndUnSTwFxX88i8klK1wPXqmTgi1fa5Y1FTrgtyQUHJuHjJFSM+GbfanqQRCzVepA0X1+HC2JEbFL"
	"FmzJ/cF2c+ijaQ+aIVKgPPsBus6XFUGbOTR1Xnw/eNdpw9hmfYR78CxZzt/lVo212pAE/lqzhUCAhA0W"
	"F9IZEaCJrAxtqqZUAJI8Lyp6JoGqof1x0FzlSohaEEvVIYVENhWNJb5C4sueCQQKOyMII9jky+DmbQuP"
	"QHZQxQaFdi+oIRGg6I0dhJTFsBtGAnMiKggsLDircUmsiBHfgp/UkO1V6DEzXJmIDAzDdmj9bMh1/6hZ"
	"ZXxycGCXyx6zFQkgf/cT2zhmEPjKB+l76N3PaHxz9GNY5C/B+H0YiQ3nE6l9kUkfdStTWouJWDVgEHz2"
	"/lVsLOog9Q/LRaEQFqIO3Q==";

static bool app_protector_login(checkin_feature_t feature, checkin_handle_t *handle) {
	hasp_status_t status = hasp_login(feature, vendor_code, handle);
	if (status != HASP_STATUS_OK)
	{
		PRINTF("hasp_login failed, error code %d\n", status);
		return false;
	}
	return true;
}

static bool app_protector_logout(checkin_handle_t handle) {
	hasp_status_t status = hasp_logout(handle);
	if (status != HASP_STATUS_OK)
	{
		PRINTF("hasp_logout failed, error code %d\n", status);
		return false;
	}
	return true;
}

int app_protector_read_int32(
	checkin_handle_t handle,
	buffer_filed_t filed,
	buffer_size_t offset
) {
	int value = 0;
	hasp_status_t status = hasp_read(handle, filed, offset, 4, &value);
	if (status != HASP_STATUS_OK)
	{
		PRINTF("hasp_buf_read failed, error code %d\n", status);
		return -1;
	}
	return value;
}
#endif

static pthread_mutex_t g_lock;
static bool is_inited = false;
void app_protector_init() {
	if (is_inited)
		return;
	is_inited = true;
	pthread_mutex_init(&g_lock, NULL);
}

void app_protector_uninit() {
	if (!is_inited)
		return;
	pthread_mutex_destroy(&g_lock);
	is_inited = false;
}

_SYMBOL_HIDDEN void __app_protect_decipher(char start[], char fill[]) {
	if (!start || !fill) return;
	/* check already encrypted */
	pthread_mutex_lock(&g_lock);
	unsigned int magic = *(unsigned int *)start;
	if (magic == MAGIC_CODE) {
		pthread_mutex_unlock(&g_lock);
		DPRINTF("decrypted\n");
		return;
	}

	DPRINTF("HERE %p, %p\n", start, fill);
#ifdef _MSC_VER
	HANDLE hHandle = GetCurrentModuleHandle();
	PIMAGE_DOS_HEADER dosHeader =
		(PIMAGE_DOS_HEADER)(hHandle);  // dos header
	PIMAGE_NT_HEADERS ntHeaders = (PIMAGE_NT_HEADERS)(
		((const uint8_t *)(dosHeader)) + dosHeader->e_lfanew);  //

	// compute offset
	IMAGE_DATA_DIRECTORY *reloc_entry =
		&ntHeaders->OptionalHeader
			 .DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC];
	IMAGE_BASE_RELOCATION *base_reloc =
		(IMAGE_BASE_RELOCATION *)((char *)hHandle +
					  reloc_entry->VirtualAddress);
	if (reloc_entry->VirtualAddress) {
		fix_relocations(base_reloc, reloc_entry->Size,
				(ULONG_PTR)hHandle, start, fill, 1);
	}
#endif

	size_t origin_size = ((size_t)(fill - start) + MAX_RSA_SIZE - 1) /
			     MAX_RSA_SIZE * MAX_RSA_SIZE;
	size_t cipher_size = origin_size / MAX_RSA_SIZE * RSA_KEY_SIZE;
	DPRINTF("%p %p, %08x, %08x, %08x\n", start, fill, (int)(fill - start),
		(int)origin_size, (int)cipher_size);

	RSA_CTX *ctx = NULL;

	int ret = read_pem(&ctx);
	PROT_ASSERT(ret == 0);

	// load fake private key (crt.h.in) and exchange e and d
	bigint *exchage_ptr = ctx->d;
	ctx->d = ctx->e;
	ctx->e = exchage_ptr;

	const int keysize = ctx->num_octets;

	char *output_buf = (char *)malloc(cipher_size);
	int outlen = 0;
	// FILE *f1 = fopen("out.bin", "wb");
	// fwrite(start, cipher_size, 1, f1);
	// fclose(f1);

	// RSA_print(ctx);
	// bi_print(ctx->)

	for (size_t i = 0; i < cipher_size / keysize; i++) {
		// int ret = RSA_decrypt(ctx, (const uint8_t *)start+i*keysize,
		// (uint8_t *)output_buf+outlen, 1);
		// use modified decrypt function
		int ret = RSA_decrypt_clone(
			ctx, (const uint8_t *)start + i * keysize,
			(uint8_t *)output_buf + outlen, 1);
		if (ret < 0) {
			DPRINTF("%d, ret < 0\n", i);
			break;
		}
		outlen += ret;
	}
	// f1 = fopen("out_de.bin", "wb");
	// fwrite(output_buf, cipher_size, 1, f1);
	// fclose(f1);
	void *start_addr = (void *)((uintptr_t)start & ~0xfff);
	const size_t outlen_page =
		((uintptr_t)(start + outlen) + 4096 - 1) / 4096 * 4096 -
		(uintptr_t)start_addr;
	DPRINTF("start_addr, %p, outlen_page: %d\n", start_addr,
		(int)outlen_page);

#ifdef _MSC_VER
	DWORD oldProt = 0, newProt = 0;
	BOOL winRet = VirtualProtect(start_addr, outlen_page,
				     PAGE_EXECUTE_READWRITE, &oldProt);
	PROT_ASSERT(winRet != 0);
	memcpy(start, output_buf, outlen);
	winRet = VirtualProtect(start_addr, outlen_page, oldProt, &newProt);
	PROT_ASSERT(winRet != 0);
#else
	ret = mprotect(start_addr, outlen_page,
		       PROT_WRITE | PROT_READ | PROT_EXEC);
	PROT_ASSERT(ret == 0);
	memcpy(start, output_buf, outlen);
	mprotect(start_addr, outlen_page, PROT_READ | PROT_EXEC);
#endif

	free(output_buf);
	RSA_free(ctx);

#ifdef _MSC_VER
	if (reloc_entry->VirtualAddress) {
		fix_relocations(base_reloc, reloc_entry->Size,
				(ULONG_PTR)hHandle, start, fill, 0);
	}
#endif

#if (defined __arm__) || (defined __aarch64__)
#if defined(__APPLE__) && defined(__MACH__)
	sys_icache_invalidate(start, (char *)fill - (char *)start);
#else
	__clear_cache(start, fill);
#endif
#endif
	magic = *(unsigned int *)start;
	if (magic == MAGIC_CODE) {
		DPRINTF("done\n");
		pthread_mutex_unlock(&g_lock);
		return;
	} else {
		DPRINTF("fail to decrypt\n");
		pthread_mutex_unlock(&g_lock);
		exit(1);
	}
}

static inline int validate_date(unsigned int date) {
	if (date < 0) return -1;
	if (date % 100 > 31 || date % 100 == 0) return -1;
	if (date % 10000 / 100 > 12 || date % 10000 / 100 == 0) return -1;
	return 0;
}

// check time limit
static inline bool app_protector_checktime() {
	time_t now;
	struct tm *w;

	time(&now);
	w = localtime(&now);

	int y = w->tm_year + 1900, m = w->tm_mon + 1, d = w->tm_mday;
	unsigned int data = y * 10000 + m * 100 + d;

	char text[4];
	memcpy(text, &crtfct.start_date, sizeof(unsigned int));
	exchange(text, 4);
	unsigned int t1 = *(unsigned int *)text;

	memcpy(text, &crtfct.end_date, sizeof(unsigned int));
	exchange(text, 4);
	unsigned int t2 = *(unsigned int *)text;

	DPRINTF("data: %d, t1:%d, t2:%d\n", data, t1, t2);

	int len = strlen((const char *)tfdt);
	char *err_msg_date = (char *)malloc(len + 1);
	strcpy(err_msg_date, (const char*)tfdt);
	exchange(err_msg_date, len);

	if (validate_date(t1) < 0 || validate_date(t2) < 0) {
		free(err_msg_date);
		// exit(1);
		return false;
	}
	if (data < t1 || data > t2) {
		PRINTF("%s\n", err_msg_date);
#ifdef _MSC_VER
		HANDLE hEventLog = NULL;
		if ((hEventLog = RegisterEventSource(
			     NULL, TEXT("APP_PROTECTOR"))) != NULL) {
			LPCTSTR msgStrings[] = {err_msg_date};
			ReportEvent(hEventLog, EVENTLOG_INFORMATION_TYPE, 0, 0,
				    NULL, 1, 0, msgStrings, NULL);
		}
		if (hEventLog) {
			DeregisterEventSource(hEventLog);
		}
#endif
		free(err_msg_date);
		// exit(1);
		return false;
	}

	free(err_msg_date);
	return true;
}
// check original appid for default running
static int check_origin_id(char *appid) {
	int len = strlen(appid);
	if (len != 24) return -1;
	if (appid[0] != 's') return -1;
	if (appid[1] != 'e') return -1;
	if (appid[2] != 'n') return -1;
	if (appid[3] != 's') return -1;
	if (appid[4] != 'e') return -1;
	if (appid[5] != 't') return -1;
	if (appid[6] != 'i') return -1;
	if (appid[7] != 'm') return -1;
	if (appid[8] != 'e') return -1;
	if (appid[9] != 'i') return -1;
	if (appid[10] != 's') return -1;
	if (appid[11] != 'a') return -1;
	if (appid[12] != 'g') return -1;
	if (appid[13] != 'r') return -1;
	if (appid[14] != 'e') return -1;
	if (appid[15] != 'a') return -1;
	if (appid[16] != 't') return -1;
	if (appid[17] != 'c') return -1;
	if (appid[18] != 'o') return -1;
	if (appid[19] != 'm') return -1;
	if (appid[20] != 'p') return -1;
	if (appid[21] != 'a') return -1;
	if (appid[22] != 'n') return -1;
	if (appid[23] != 'y') return -1;
	return 0;
}

static bool found_in_union_pattern(const char* expected_union_pattern, const char* input) {
	if (!expected_union_pattern || !expected_union_pattern[0] || !input || !input[0]) {
		return false;
	}
	std::string str_to_match(input);
	std::string union_pattern(expected_union_pattern);
	char delim = '|';
	// split the pattern
	std::vector<std::string> patterns;
	std::stringstream ss(union_pattern);
	std::string item;
	while (std::getline(ss, item, delim)) {
		patterns.push_back(item);
	}
	for (std::vector<std::string>::iterator it = patterns.begin(); it != patterns.end(); ++it) {
		if (*it == str_to_match) {
			return true;
		}
	}
	return false;
}

// check app id
static inline bool app_protector_checkappid(const char *actual_appid) {
	int len = strlen((char*)crtfct.appid);
	char *appid = (char *)malloc(len + 1);
	strcpy(appid, (char*)crtfct.appid);
	exchange(appid, len);
	DPRINTF("appid: %s\n", appid);
	DPRINTF("input string: %s\n", actual_appid);

	len = strlen((const char *)wrongid);
	char *err_msg_id = (char *)malloc(len + 1);
	strcpy(err_msg_id, (char*)wrongid);
	err_msg_id[len] = '\0';
	exchange(err_msg_id, len);

	bool ret = true;
	if (check_origin_id(appid) != 0) {
		if(!found_in_union_pattern(appid, actual_appid)) {
			ret = false;
		}
	}
	free(appid);
	free(err_msg_id);
	return ret;
}

int app_protector_check() {
	bool check_re = true;
	check_re = check_re && app_protector_checktime();

#if (defined(__APPLE__) && defined(__MACH__)) || defined(__ANDROID__)
	char buf[MAX_INFO_LENGTH];
#if defined(__APPLE__) && defined(__MACH__)
	(void)mach_get_app_id(buf);
#elif defined(__ANDROID__)
	(void)linux_get_app_id(buf);
#endif
	check_re = check_re && app_protector_checkappid(buf);
	DPRINTF("App: %s\n", buf);
#endif
	return check_re;
}

int app_protector_checkin(checkin_feature_t feature_id, checkin_handle_t *handle) {
	bool check_re = true;
	*handle = 0;
#if CONFIG_PROTECTOR_WITH_HASP
	bool need_logout = false;
	checkin_handle_t hasp_hdl;
	check_re = check_re && app_protector_login(feature_id, &hasp_hdl);
	if (check_re) {
		need_logout = true;
	}
#endif
	check_re = check_re && app_protector_check();

#if CONFIG_PROTECTOR_WITH_HASP
	if (!check_re && need_logout) {
		hasp_logout(hasp_hdl);
	}
	else {
		*handle = hasp_hdl;
	}
#endif
	return check_re;
}

int app_protector_checkout(checkin_handle_t handle) {
	bool check_re = true;
#if CONFIG_PROTECTOR_WITH_HASP
	if (handle) {
		check_re = check_re && app_protector_logout(handle);
	}
#endif
	return check_re;
}

void app_protector_send_init_log(const char *module, const char *version,
				 const char *channel) {
}

#define STEF_STARTCODE_LEN	(4)
#define STEF_PADDING_LEN	(8)
const char stef_startcode[] = "STEF";

#define STEF_HEADER_LEN		(STEF_STARTCODE_LEN + STEF_PADDING_LEN + sizeof(int32_t))

//
// Encrypt file format
//
// |---- 4 BYTES ----|---- 4 BYTES ----|---- 4 BYTES ----|---- 4 BYTES ----|
// | 'S' 'T' 'E' 'F' | encrypt-data len|          padding bytes            |
// |-----------------------------------------------------------------------|
// | encrypt data....                                                      |
// |                                                                       |
// |-----------------------------------------------------------------------|
//

// read encrypt key & iv
static inline int read_aes_key(AES_CTX *aes_ctx) {
	PROT_ASSERT(aes_ctx != NULL);

	int key_len = strlen((const char *)aes_key);
	uint8_t *key = (uint8_t *)calloc(1, key_len + 1);
	memcpy(key, aes_key, key_len);
	exchange((char*)key, key_len);

	int iv_len = strlen((const char *)aes_iv);
	uint8_t *iv = (uint8_t *)calloc(1, iv_len + 1);
	memcpy(iv, aes_iv, iv_len);
	exchange((char*)iv, iv_len);

	AES_set_key(aes_ctx, key, iv, AES_MODE_128);
	free(key);
	free(iv);

	return 0;
}

int STEF_is_encrypt_file(const char *fn)
{
	bool ret = false;
	FILE *f = fopen(fn, "rb");
	if (f != NULL) {
		char start_code[STEF_STARTCODE_LEN];
		int len = fread(start_code, 1, STEF_STARTCODE_LEN, f);
		if (len == STEF_STARTCODE_LEN) {
			ret = strncmp(start_code, stef_startcode, STEF_STARTCODE_LEN) == 0;
		}
		fclose(f);
	}
	return ret;
}

int STEF_is_encrypt_memory(const unsigned char *mem)
{
	bool ret = false;
	if (mem != NULL) {
		ret = strncmp((const char*)mem, stef_startcode, STEF_STARTCODE_LEN) == 0;
	}
	return ret;
}

int STEF_get_original_size(const unsigned char *mem)
{
	int len = 0;
	if(mem) {
		memcpy(&len, mem + STEF_STARTCODE_LEN, sizeof(int32_t));
	}
	return len;
}

int STEF_decrypt2mem(unsigned char *start, unsigned char *fill,
		unsigned char **output_buf) {
	if (!start || !fill) {
		return -1;
	} else {
		unsigned char *pos = start;
		if(!STEF_is_encrypt_memory(pos)) {
			return -1;
		}

		int len = STEF_get_original_size(pos);
		pos += STEF_HEADER_LEN;

		int buf_len = (len + 15) / 16 * 16;
		if ((buf_len + STEF_HEADER_LEN) > (fill - start)) {
			return -1;
		}
		uint8_t *buf = pos;
		if (*output_buf == NULL) {
			buf = (unsigned char *)malloc(buf_len);
			if (buf == NULL) {
				return -1;
			}
			memset(buf, 0, buf_len);
		}

		AES_CTX ctx;
		read_aes_key(&ctx);
		AES_convert_key(&ctx);

		AES_cbc_decrypt(&ctx, pos, buf, buf_len);
		*output_buf = buf;
		return len;
	}
}

int STEF_encrypt2mem(unsigned char *start, unsigned char *fill,
		unsigned char **output_buf) {

	*output_buf = NULL;

	if (!start || !fill) {
		return -1;
	} else {
		int32_t len, aligned_len, out_len;
		AES_CTX ctx;
		uint8_t *buf = NULL, *pos = NULL;

		read_aes_key(&ctx);

		len = fill - start;
		aligned_len = (len + 15) / 16 * 16;
		out_len = aligned_len + STEF_STARTCODE_LEN + sizeof(int32_t) + STEF_PADDING_LEN;

		pos = buf = (unsigned char *)malloc(out_len);
		if(buf == NULL)
		{
			return -1;
		}

		memcpy(pos, stef_startcode, STEF_STARTCODE_LEN);
		pos += STEF_STARTCODE_LEN;
		memcpy(pos, &len, sizeof(int32_t));
		pos += sizeof(int32_t);
		pos += STEF_PADDING_LEN;

		AES_cbc_encrypt(&ctx, start, pos, aligned_len);

		*output_buf = buf;
		return out_len;
	}
}

/**
 * Retrieve a file and put it into memory
 * @return The size of the file, or -1 on failure.
 */
int file2mem(const char *filename, uint8_t **buf) {
	int total_bytes = 0;
	int bytes_read = 0;
	int filesize;
	FILE *stream = fopen(filename, "rb");

	if (stream == NULL) {
		return -1;
	}

	fseek(stream, 0, SEEK_END);
	filesize = ftell(stream);
	*buf = (uint8_t *)malloc(filesize);
	fseek(stream, 0, SEEK_SET);

	do {
		bytes_read = fread(*buf + total_bytes, 1, filesize - total_bytes, stream);
		total_bytes += bytes_read;
	} while (total_bytes < filesize && bytes_read > 0);

	fclose(stream);
	return filesize;
}

