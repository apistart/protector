require 'pathname'
require 'optparse'
require 'optparse/time'

WORK_DIR = Pathname.new(File.dirname(__FILE__)).realpath.to_s

PUBLIC_KEY = WORK_DIR + '/keys/test_rsa.pem'				# path of RSA public key
ENCRYPTED_PRIVATE_KEY = WORK_DIR + '/keys/test_rsa_encrypted'		# path of encrypted private key
PRIVATE_KEY = WORK_DIR + '/keys/test_rsa'				# path of RSA private key
SET_PRIVATE_KEY = 0							# 1: set private key  0 not set private key

KEY_SIZE = 256
MAX_RSA_SIZE = KEY_SIZE - 11
TIME_OFFSET = 3072 + 4							# time offset, see details of struct _crtfct in /src/protect.h
APPID_OFFSET = 3072 + 12						# appid offset
TAG_OFFSET = 3072 + 4 * 3 + 256 * 2					# tag offset

SYM_KEY = 0x67								# key of symmetric encryption

def symm_swap(ch)
  return (((ch & 1) << 7)|((ch & (2)) << 5)|((ch & (4)) << 3)|((ch & (8)) << 1)|((ch & (16)) >> 1)|((ch & (32)) >> 3)|((ch & (64)) >> 5)|((ch & (128)) >> 7))
end

def symm_encrypt(data, n_byte=4)
  ''' encrypt data by symmetric  encryption

        data: the original data, an integer
        n_byte: bytes of the data
        return: the encrypted data
  '''
  value = 0
  for i in 0..n_byte-1
    d = data / (1<<(8*(n_byte-1-i)))
    data = data % (1<<(8*(n_byte-1-i)))
    d = symm_swap(d)
    d ^= SYM_KEY
    value = (value << 8) + d
  end
  return value
end

def symm_encrypt_str(str)
  ''' encrypt string by symmetric  encryption

        str: the original string
        return: the encrypted string
  '''
  st = ""
  st = str.bytes.map{|c| (symm_swap(c) ^ SYM_KEY) }.pack("C*")
  return st
end

class ProtectorOptParser

  #
  # Return a structure describing the options.
  #
  def self.parse!(args)
    # The options specified on the command line will be collected in *options*.
    # We set default values here.
    options = {}

    opts = OptionParser.new do |opts|
      opts.banner = "Usage: #{$0} [options] file"

      opts.separator ""
      opts.separator "Specific options:"

      opts.on("-t", "--timelimit [start_date,]end_date",Array,
              "Set timelimit to *.so","(Example:19700101,20991231)") do |time|
        unless time.nil?
          if time.size == 1
            sd = 19700101
            ed = Integer(time[0])
          else
            sd = Integer(time[0])
            ed = Integer(time[1])
          end
          fail 'end_date < start_date, invalid' if ed < sd
          options[:start_date] = sd
          options[:end_date] = ed
          options[:timelimit] = true
        end
      end

      opts.on("-a", "--appid ID","Set appid to *.so","(Example:com.sensetime)") do |id|
        options[:appid] = id
      end

      opts.on("-g", "--tag TAG","Set tag to *.so","(Example:Sensetime_test_20000101)") do |tag|
        fail "tag too long" if tag.size > 63
        options[:tag] = tag
      end

      opts.on("-e", "--encrypt","Encrypt the library") do |e|
        options[:encrypted] = true
      end

      opts.on("-o", "--overwrite","Overwrite the file") do |o|
        options[:overwrite] = true
      end

      opts.separator ""
      opts.separator "Common options:"

      # No argument, shows at tail.This will print an options summary.
      # Try it and see!
      opts.on_tail("-h", "--help", "Show this message") do
        puts opts
        exit
      end

    end

    opts.parse!(args)
    fail 'no input file' if args.size == 0
    options
  end

end
