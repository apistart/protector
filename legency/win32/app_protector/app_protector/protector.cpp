#include "protector.h"

#pragma comment(lib,"protector.lib")

DllExport int stest(int x) {
	return x + x;
}

int APP_PROTECTOR_FUNC goo(int x)
{
	static int k = 3;
	APP_PROTECTOR_START(goo)
		x = x+ 2 * k;
	int y = stest(x);
	APP_PROTECTOR_END(goo)

		return y+1;
}

int APP_PROTECTOR_FUNC good(int x)
{
	static int k = 3;
APP_PROTECTOR_START(good)
APP_PROTECTOR_CHECKTIME();
	x = x+ 2 * k;
	int y = stest(x);
APP_PROTECTOR_END(good)

		return y+1;
}

DllExport int expot(int x){
	return x+good(x)+goo(x);
}