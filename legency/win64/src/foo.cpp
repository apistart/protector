#include <protector.h>

#pragma comment(lib,"protector.lib")

#ifdef _MSC_VER
#define EXPORT_SYMBOL __declspec(dllexport)
#endif

EXPORT_SYMBOL int stest(int x) {
	return x + x;
}

EXPORT_SYMBOL int goo(int x)
{
	static int k = 3;
	APP_PROTECTOR_START(goo)
		x = x+ 2 * k;
	int y = stest(x);
	APP_PROTECTOR_END(goo)

		return y+1;
}

int good(int x)
{
	static int k = 3;
APP_PROTECTOR_START(good)
APP_PROTECTOR_CHECKTIME();
	x = x+ 2 * k;
	int y = stest(x);
APP_PROTECTOR_END(good)

		return y+1;
}

extern "C" EXPORT_SYMBOL int expot(int x){
	return x+good(x)+goo(x);
}
