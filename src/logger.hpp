#ifndef LOGGER_H_YSX8RTLC
#define LOGGER_H_YSX8RTLC

#include <vector>
#include <string>
#include <utility>

typedef std::pair<std::string, std::string> LogKeyValue;
void send_log_async(const char* host, const char *url,
		const std::vector<LogKeyValue> &kv);

#endif /* end of include guard: LOGGER_H_YSX8RTLC */

