#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "readinfo.h"

#if defined(__APPLE__) && defined(__MACH__)
#include <unistd.h>
#include <sys/sysctl.h>
#endif

#define PROPERTY_VALUE_MAX  92

#if __ANDROID__
#include <sys/system_properties.h>

#if __ANDROID_API__ >= 21
/* https://src.chromium.org/viewvc/chrome/trunk/src/base/sys_info_android.cc?pathrev=284012
 * __system_property_get removed on __ANDROID_API__ 21
 */
#include <dlfcn.h>
typedef int (SystemPropertyGetFunction)(const char*, char*);

int __system_property_get(const char *key, char *v) {
	static SystemPropertyGetFunction* real_system_property_get = 0;
	if(!real_system_property_get) {
		void* handle = dlopen("libc.so", RTLD_NOLOAD);
		if(!handle)
			return 0;
		real_system_property_get = (SystemPropertyGetFunction*)
				dlsym(handle, "__system_property_get");
	}
	if(real_system_property_get)
		return real_system_property_get(key, v);
	else
		return 0;
}
#endif
/* system/core/libcutils/properties.c */
/* http://osxr.org/android/source/frameworks/base/core/java/android/os/Build.java
 */
int property_get(const char *key, char *value, const char *default_value)
{
	int len;

	len = __system_property_get(key, value);
	if(len > 0) {
		return len;
	}

	if(default_value) {
		len = strlen(default_value);
		memcpy(value, default_value, len + 1);
	}
	return len;
}
#endif

#if __linux__
/* wlan0, p2p0, eth0 */
/* not available when Wifi shutdown */
int linux_get_mac(char *val)
{
	/* /sys/class/net/eth1/address */
	int i;
	const char *devs[] = {"eth0", "eth1", "wlan0", "p2p0"};
	const int dev_len = sizeof(devs) / sizeof(char*);
	char buf[128];
	for(i = 0; i < dev_len; i++) {
		sprintf(buf, "/sys/class/net/%s/address", devs[i]);
		FILE *f = fopen(buf, "r");
		if(!f)
			continue;
		size_t l = fread(buf, 1, 64, f);
		fclose(f);
		if(l <= 0)
			continue;
		buf[l] = 0;
		/* remove newline */
		if(l >= 1 && buf[l-1] == '\n')
			buf[l-1] = 0;
		strcpy(val, buf);
		return l;
	}
	/* not found */
	val[0] = 0;
	return 0;
}

/* https://github.com/android/platform_system_core/blob/master/toolbox/ps.c
 */
int linux_get_app_id(char *val)
{
	const char *cmdline = "/proc/self/cmdline";
	char buf[MAX_INFO_LENGTH];
	int r;
	FILE *f = fopen(cmdline, "r");
	if(f) {
		r = fread(buf, 1, MAX_INFO_LENGTH-1, f);
		buf[r] = 0;
		strcpy(val, buf);
		fclose(f);
		return r;
	}
	val[0] = 0;
	return 0;
}

#endif

#if defined(__APPLE__) && defined(__MACH__)
/* XXX executable path differs in every start
 * Do we have any better solution to get Bundle ID?
 */
int mach_get_app_path(char *val)
{
	int             mib[3], argmax, nargs;
	size_t          size;
	char            *procargs, *cp;
	const char *prefix = "/private/var/mobile/Containers/Bundle/Application/";
	const int prefix_len = strlen(prefix);
	/*  Made into a command argument. -- TRW
	 *      extern int      eflg;
	 */

	/* Get the maximum process arguments size. */
	mib[0] = CTL_KERN;
	mib[1] = KERN_ARGMAX;

	size = sizeof(argmax);
	if (sysctl(mib, 2, &argmax, &size, NULL, 0) == -1) {
		val[0] = 0;
		return 0;
	}

	/* Allocate space for the arguments. */
	procargs = (char *)malloc(argmax);
	if (procargs == NULL)
		goto err;

	mib[0] = CTL_KERN;
	mib[1] = KERN_PROCARGS2;
	mib[2] = getpid();

	size = (size_t)argmax;
	if (sysctl(mib, 3, procargs, &size, NULL, 0) == -1)
		goto err;

	memcpy(&nargs, procargs, sizeof(nargs));

	cp = procargs + sizeof(nargs);
	if(strstr(cp, prefix))
		cp += prefix_len;

	strncpy(val, cp, MAX_INFO_LENGTH);
	val[MAX_INFO_LENGTH-1] = 0;

	free(procargs);

	return strlen(val);
err:
	free(procargs);
	val[0] = 0;
	return 0;
}
#endif

void get_os_type(char *val)
{
#if __ANDROID__
	strcpy(val, "android");
#elif __linux__
	strcpy(val, "linux");
#elif defined(_MSC_VER)
	strcpy(val, "windows");
#elif defined(__APPLE__) && defined(__MACH__)
	strcpy(val, "ios");
#else
	strcpy(val, "unknown");
#endif
}



//static inline void get

#if 0
int main(int argc, char const *argv[])
{

	int ret = 0;
	char buf[PROPERTY_VALUE_MAX];
	ret = property_get("ro.serialno",  buf, "");

	fprintf(stderr, "Serial: %s\n", buf);

	ret = linux_get_mac(buf);
	fprintf(stderr, "MAC: %s\n", buf);

	ret = linux_get_app_id(buf);
	fprintf(stderr, "App: %s\n", buf);
	return 0;
}
#endif 
