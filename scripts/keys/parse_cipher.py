import sys


if __name__ == '__main__':
    if len(sys.argv) > 2:
        print "too much parameters"
        exit(-1)
  
    filePath = sys.argv[1]
    f = open(filePath)
    st = f.read()
    st = st.encode("hex")
    f.close()
    
    f = open("hex.txt",'w')

    i = 0;
    length = len(st)

    while i < length:
	s = st[i:i+2]
	f.write("0x%s," % s)
	i += 2
	if i % 30 == 0:
            f.write("\n")
    f.close()

