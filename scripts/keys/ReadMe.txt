RSA 密钥 以及对其进行对称加密的脚本

目录结构：
 test_rsa: 私钥
 test_rsa_pem: 公钥
 encrypt.cpp: 对私钥进行对称加密的脚本,会生成 test_rsa_encrypted 加密后的文件，可以修改 文件中的 KEY, 相应的需要修改 ../app_protect.rb 中的 KEY, 以及 protect.h 中的 KEY, 三者要保持一致
 parse_cipher: 解析加密后的文件成 hex 编码，从而保存在 hex.txt 中，可以直接复制到src/protect.cpp 中
