#include <iostream>
#include <windows.h>
using namespace std;

typedef int(*TEST )(int);
int main( char argc, char* argv[] ) {         
	const char* dllName = argv[1];
	const char* funcName = "expot";

	HMODULE hDLL = LoadLibrary(dllName);
	if ( hDLL != NULL ) {
		TEST func = TEST( GetProcAddress( hDLL, funcName ) );
		if ( func != NULL ) {
			cout << func(10) << endl;
		}
		else {
			cerr << "Unable to find function /'" << funcName << "/' !" << endl;
		}
		FreeLibrary( hDLL );
	} 
	else {
		cout << "Unable to load DLL '" << dllName << "'  " << GetLastError() << endl;
	}    
	return 0;
}
