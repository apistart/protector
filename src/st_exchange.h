#ifndef EXCHANGE_H_HVWA1R8W
#define EXCHANGE_H_HVWA1R8W

#define SYMMETRIC_KEY  0x67

/* symmetric bitwise swapping */
#define SYMM_SWAP(ch)                                                  \
	(((ch & 1) << 7) | ((ch & (2)) << 5) | ((ch & (4)) << 3) |     \
	 ((ch & (8)) << 1) | ((ch & (16)) >> 1) | ((ch & (32)) >> 3) | \
	 ((ch & (64)) >> 5) | ((ch & (128)) >> 7))

// symmetric decryption
static inline void exchange(char *text, int len) {
	for (int i = 0; i < len; i++) {
		text[i] ^= SYMMETRIC_KEY;
		text[i] = SYMM_SWAP(text[i]);
	}
}


#endif /* end of include guard: EXCHANGE_H_HVWA1R8W */
