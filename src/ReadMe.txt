title: SDK保护的工具，主要完成代码段加密、日期验证等功能

-crypto/
	跨 Windows(VS 2012)、Linux、Android 平台的第三方的加解密库，本项目主要使用其 RSA 解密的功能 RSA_decrypt 函数

-protector.h
 跨平台的头文件，包含许多内联汇编代码、三个重要的宏定义 APP_PROTECTOR_START、APP_PROTECTOR_END、APP_PROTECTOR_CHECKTIME

-protector.cpp
 代码的具体实现
 exchange 函数完成了对称解密功能
 crtfct 字符串是 RSA private key 的加密版本，可以在 /encrpyt/*/keys/hex.txt 里面获得
 fix_relocations 主要是解决代码段中的重定位问题，在代码段解密前后分别调用一次
 

=========

Warning for Win32:
- Only EXPORT functions can be encrypt
- All EXPORTs are assumed to be consecutive in PE Image
- The last EXPORT functions act as the boundary and never get encrypted!
- Encrypted function should not larger than 64 pages...

TODO:
- Win32
	* find OLD_IMAGE_BASE
	* find __appProtector_* symbols without hard code offset

- Win64
	* NOT SUPPORTED AT ALL!!


