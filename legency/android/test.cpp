#include <stdio.h>
#include <protector.h>

int bar(int x) { printf("XX %d\n", x); return x + 4;}
int main(int argc, char const *argv[])
{	

	int x = 4;
	asm volatile (
		"nop"
	);
	APP_PROTECTOR_START(main)
	bar(x+4);
	APP_PROTECTOR_END(main)
	//send_app_id("sensetime&kedaxunfei");
	//printf("%d\n", bar(10));
	return 0;
}
