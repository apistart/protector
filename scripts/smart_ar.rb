#!/usr/bin/env ruby

# this file is copied from cppbuild-rs, keep sync
# with that version to get bug fixes!
require 'fileutils'

FIELD_LENS=[16,12,6,6,8,10,2]

def check_ar_type inputfn
  type = :unknown
  File.open(inputfn, "rb") {|io|
    magic = io.read(8)
    fail "not ar file" unless magic == "!<arch>\n"
    until io.eof?
      info = []
      FIELD_LENS.each {|l| info << io.read(l)}
      type = info[0].strip.start_with?('#') ? :bsd : :gnu
      break
    end
  }
  type
end

def unpack_ar_bsd(inputfn, outputdir)
	File.open(inputfn, "rb") {|io|
		magic = io.read(8)
		fail "not ar file" unless magic == "!<arch>\n"
		cnt = 0
		until io.eof?
			info = []
			FIELD_LENS.each {|l| info << io.read(l)}
			#p info
			fail "corrupted file" unless info.last == "`\n"
			fn = info[0].strip
			size = info[-2].strip.to_i
			if fn =~ /#1\/(\d+)\s*/
				fn_len = $1.to_i
				fn = io.read(fn_len).gsub("\x00", "")
				size -= fn_len
			end
			#p [fn, size]
			content = io.read size
			File.open("#{outputdir}/#{"%04d-%s" % [cnt,fn]}", "wb") {|wio| wio.write content}
			io.read(1) if size & 0x1 == 1
			cnt += 1
		end
	}
end

def unpack_ar_linux(inputfn, outputdir)
  extend_table = nil
	File.open(inputfn, "rb") {|io|
		magic = io.read(8)
		fail "not ar file" unless magic == "!<arch>\n"
		cnt = 0
		until io.eof?
			info = []
			FIELD_LENS.each {|l| info << io.read(l)}
			#p info
			fail "corrupted file" unless info.last == "`\n"
			fn = info[0].rstrip
			size = info[-2].strip.to_i
			content = io.read size
			io.read(1) if size & 0x1 == 1
      if fn == '//'
        # extended file name record
        extend_table = content
      elsif fn == '/'
        # skip
      else
        if fn.start_with? '/'
          # reference
          fail 'extended filenames not available' unless extend_table
          idx = fn[1..-1].to_i
          fn = extend_table[idx..-1].split('/').first
        else
          fail 'filename not ended with /' unless fn.end_with? '/'
          fn = fn[0..-2]
        end
        #p fn
			  File.open("#{outputdir}/#{"%04d-%s" % [cnt,fn]}", "wb") {|wio| wio.write content}
			  cnt += 1
      end
		end
	}
end

def unpack_ar(inputfn, outputdir)
  FileUtils.mkdir_p outputdir

  type = check_ar_type inputfn
  #STDERR.puts "archive_type: #{type}"
  case type
  when :gnu
    unpack_ar_linux inputfn, outputdir
  when :bsd
    unpack_ar_bsd inputfn, outputdir
  else
    fail "Unknown archive type: #{type}"
  end
end

if __FILE__ == $0
  if ARGV.size != 2
    puts "Usage: #{$0} [input file] [output dir]"
    exit 1
  end

  fail "#{ARGV[0]} not found" unless File.file? ARGV[0]

  unpack_ar ARGV[0], ARGV[1]
end
