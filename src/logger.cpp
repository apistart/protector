#include <cstring>
#include <cstdio>
#include <string>
#include <utility>
#include <vector>

#ifndef _MSC_VER
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <unistd.h>
#else
#define snprintf _snprintf
#define sleep(x) Sleep((x) * 1000)
#define close(x) closesocket(x)
#include <WS2tcpip.h>
#include <windows.h>
#pragma comment(lib, "ws2_32.lib")
#endif

#include "logger.hpp"
#include "protector/crypto/crypto.h"

#define PING_SERVER_PORT 80

struct request {
	std::string host;
	std::string url;
	std::string body;
};

static const char *body_key = "SenseTime-API";

static bool log_inited = false;

static const char Base64[] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char Pad64 = '=';

static int b64_ntop(unsigned char const *src, size_t srclength, char *target,
		    size_t targsize) {
	size_t datalength = 0;
	unsigned char input[3];
	unsigned char output[4];
	size_t i;

	while (2 < srclength) {
		input[0] = *src++;
		input[1] = *src++;
		input[2] = *src++;
		srclength -= 3;

		output[0] = input[0] >> 2;
		output[1] = ((input[0] & 0x03) << 4) + (input[1] >> 4);
		output[2] = ((input[1] & 0x0f) << 2) + (input[2] >> 6);
		output[3] = input[2] & 0x3f;

		if (datalength + 4 > targsize) return (-1);
		target[datalength++] = Base64[output[0]];
		target[datalength++] = Base64[output[1]];
		target[datalength++] = Base64[output[2]];
		target[datalength++] = Base64[output[3]];
	}

	/* Now we worry about padding. */
	if (0 != srclength) {
		/* Get what's left. */
		input[0] = input[1] = input[2] = '\0';
		for (i = 0; i < srclength; i++) input[i] = *src++;

		output[0] = input[0] >> 2;
		output[1] = ((input[0] & 0x03) << 4) + (input[1] >> 4);
		output[2] = ((input[1] & 0x0f) << 2) + (input[2] >> 6);

		if (datalength + 4 > targsize) return (-1);
		target[datalength++] = Base64[output[0]];
		target[datalength++] = Base64[output[1]];
		if (srclength == 1)
			target[datalength++] = Pad64;
		else
			target[datalength++] = Base64[output[2]];
		target[datalength++] = Pad64;
	}
	if (datalength >= targsize) return (-1);
	target[datalength] = '\0'; /* Returned value doesn't count \0. */
	return (datalength);
}


#ifdef _MSC_VER
static DWORD WINAPI send_thread(LPVOID ptr)
#else
static void *send_thread(void *ptr)
#endif
{

	RC4_CTX rc4_ctx;
	struct request *req = static_cast<struct request *>(ptr);
	struct hostent *host = 0;
	struct in_addr **addr_list = 0;
	char ip[64] = {0};
	char post[2048];
	unsigned char body[1024];

#ifdef _MSC_VER
	WSADATA  Ws;
	if (WSAStartup(MAKEWORD(2, 2), &Ws) != 0) {
		delete req;
		return -1;
	}
#endif

	RC4_setup(&rc4_ctx, (unsigned char *)body_key, strlen(body_key));
	size_t bodylen = req->body.size();
	if (bodylen > 1023) bodylen = 1023;
	memcpy(body, req->body.c_str(), bodylen);
	body[bodylen] = 0;
	RC4_crypt(&rc4_ctx, body, body, bodylen);

	char body_base64[2048];
	int body_base64_len = b64_ntop(body, bodylen, body_base64, 2048);
	/* never true */
	if (body_base64_len <= 0) {
		delete req;
		return NULL;
	}

	int post_len = snprintf(post, sizeof(post),
				"POST %s HTTP/1.1\r\n"
				"Host: %s\r\n"
				"User-Agent: SenseTime Ping Agent 0.1\r\n"
				"Content-Length: %d\r\n"
				"\r\n"
				"%s",
				req->url.c_str(), req->host.c_str(),
				body_base64_len, body_base64);

	// fprintf(stderr, "HERE\n");
	for (int i = 0; i < 5; i++) {
		/* DNS */
		if (!ip[0]) {
			host = gethostbyname(req->host.c_str());
			if (!host) goto retry;
			addr_list = (struct in_addr **)host->h_addr_list;
			if (addr_list[0] != 0) {
				strncpy(ip, inet_ntoa(*addr_list[0]), 63);
			} else {
				goto err;
			}
		}
		// fprintf(stderr, "IP %s\n", ip);
		int sockfd;
		struct sockaddr_in serv_addr;
		if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) goto retry;
		memset(&serv_addr, 0, sizeof(serv_addr));

		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(PING_SERVER_PORT);
		if (inet_pton(AF_INET, ip, &serv_addr.sin_addr) <= 0) goto err;
		if (connect(sockfd, (struct sockaddr *)&serv_addr,
			    sizeof(serv_addr)) < 0) {
			close(sockfd);
			goto retry;
		}
		if (send(sockfd, post, post_len, 0) < 0) {
			close(sockfd);
			goto retry;
		}
		close(sockfd);
		break;
	retry:
		// fprintf(stderr, "retry\n");
		sleep(10);
	}
err:
	delete req;
	return NULL;
}

void send_log_async(const char *hostname, const char *url,
	const std::vector<LogKeyValue> &kv) {
	if (!log_inited) {
		log_inited = true;
	}
	std::string body = "{";
	for (size_t i = 0; i < kv.size(); i++) {
		body.push_back('"');
		body += kv[i].first;
		body += "\":\"";
		body += kv[i].second;
		body.push_back('"');
		if (i < kv.size() - 1) body.push_back(',');
	}
	body += "}";
	struct request *req = new request();
	req->host = hostname;
	req->url = url;
	req->body = body;

#ifndef _MSC_VER
	pthread_t tid = 0;
	int r = pthread_create(&tid, NULL, send_thread, req);
	if (r != 0) {
		delete req;
		return;
	}
	pthread_detach(tid);
#else
	HANDLE handle = NULL;
	handle = CreateThread(NULL, 0, send_thread, req, 0, NULL);
	if (handle == NULL) {
		delete req;
		return;
	}
	CloseHandle(handle);
#endif
}
